// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_STATEMENT
#define COLLISIONLIBRARY_STATEMENT
#include "collisionlibrary"


inline bool error_statement(const std::string& msg) {
	if (lib::libSpeaker.get_outOp() != lib::OutputOption::SILENT) { // print as long as we are not SILENT
		std::cout << "\ncollisionlibrary: *** ERROR *** : " << msg;
		fflush(stdout);
	}

	return true;
}

inline bool normal_statement(const std::string& msg) {
	if (lib::libSpeaker.get_outOp() != lib::OutputOption::SILENT) { // print as long as we are not SILENT
		std::cout << "\ncollisionlibrary: " << msg;
		fflush(stdout);
	}

	return true;
}

inline bool debug_statement(const std::string& msg) {

	if (lib::libSpeaker.get_outOp() == lib::OutputOption::DEBUG) { //  print with special tag for debug specifically
		std::cout << "\ncollisionlibrary: debug: " << msg;
		fflush(stdout);
	}
	//else if (lib::libSpeaker.get_outOp() == lib::OutputOption::NORMAL) {
	//	lib::normal_statement(msg);
	//}

	return true;
}

#endif