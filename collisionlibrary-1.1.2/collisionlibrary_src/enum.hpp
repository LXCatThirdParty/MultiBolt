// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_ENUM
#define COLLISIONLIBRARY_ENUM
#include "collisionlibrary"

static const std::string UNALLOCATED_STRING = "UNALLOCATED";


static const std::string ATTACHMENT_STRING = "ATTACHMENT";
static const std::string ELASTIC_STRING = "ELASTIC"; // this is to remove the chance, while developing, that you accidentally spelled the string wrong
static const std::string EXCITATION_STRING = "EXCITATION";

static const std::string IONIZATION_STRING = "IONIZATION";
static const std::string EFFECTIVE_STRING = "EFFECTIVE";

static const std::string SUPERELASTIC_STRING = "SUPERELASTIC";


// Dev note: The LXCat Xsec form for rotational, vibrational-labeled Xsecs is inconsistent with those
// in excitations. CollisionLibrary will do its best to read them, but USE AT YOUR OWN RISK 1/2/2023

static const std::string ROTATIONAL_STRING = "ROTATIONAL";
static const std::string VIBRATIONAL_STRING = "VIBRATIONAL";




typedef enum CollisionCode {

	attachment,
	elastic,
	effective,
	excitation,
	ionization,
	superelastic,
	rotational,
	nocollision,
	vibrational

} CollisionCode;

std::map <lib::CollisionCode, std::string> collision_code_str_map =
							{ {lib::CollisionCode::attachment, lib::ATTACHMENT_STRING},
							{lib::CollisionCode::elastic, lib::ELASTIC_STRING},
							{lib::CollisionCode::effective, lib::EFFECTIVE_STRING},
							{lib::CollisionCode::excitation, lib::EXCITATION_STRING},
							{lib::CollisionCode::ionization, lib::IONIZATION_STRING},
							{lib::CollisionCode::superelastic, lib::SUPERELASTIC_STRING},
							{lib::CollisionCode::rotational, lib::ROTATIONAL_STRING},
							{lib::CollisionCode::nocollision, lib::UNALLOCATED_STRING}, 
							{lib::CollisionCode::vibrational, lib::VIBRATIONAL_STRING} };

// the reverse of the above
std::map <std::string, lib::CollisionCode> collision_str_code_map =
											{ {lib::ATTACHMENT_STRING, lib::CollisionCode::attachment},
											{lib::ELASTIC_STRING, lib::CollisionCode::elastic},
											{lib::EFFECTIVE_STRING, lib::CollisionCode::effective},
											{lib::EXCITATION_STRING, lib::CollisionCode::excitation},
											{lib::IONIZATION_STRING, lib::CollisionCode::ionization},
											{lib::SUPERELASTIC_STRING, lib::CollisionCode::superelastic},
											{lib::ROTATIONAL_STRING, lib::CollisionCode::rotational},
											{lib::UNALLOCATED_STRING, lib::CollisionCode::nocollision},
											{lib::VIBRATIONAL_STRING, lib::CollisionCode::vibrational} };




typedef enum OutputOption {

	NORMAL,
	DEBUG,
	SILENT

} OuputOption;

typedef enum class ScatteringCode {

	Isotropic,
	IdealForward,
	ScreenedCoulomb

} ScatteringCode;

typedef enum class InterpolationMethod {
	Linear,
	Logarithmic
} InterpolationMethod;

const InterpolationMethod DEFAULT_INTERP_METHOD = InterpolationMethod::Linear;

const std::map<lib::ScatteringCode, std::string> scattering_map = { {lib::ScatteringCode::Isotropic, "Isotropic"},
																		{lib::ScatteringCode::IdealForward, "IdealForward"},
																		{lib::ScatteringCode::ScreenedCoulomb, "ScreenedCoulomb"} };


const std::map<lib::InterpolationMethod, std::string> interp_map = { {InterpolationMethod::Linear, "Linear"},
																		{InterpolationMethod::Logarithmic, "Logarithmic"} };

#endif
