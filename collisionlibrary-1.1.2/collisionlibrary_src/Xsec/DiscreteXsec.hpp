// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_DISCRETETXSEC
#define COLLISIONLIBRARY_DISCRETEXSEC
#include "collisionlibrary"

// Xsecs whose evaluation style is determined by their datas interaction with
// a grid, knowing that they are of a certain type
// Note: Discrete is an abstract class. User 
// should specifically make one of the valid types instead.
class DiscreteXsec : public lib::AbstractXsec {
	
protected:
	arma::colvec _data_eV;
	arma::colvec _data_s;

	arma::colvec _parent_data_eV;
	arma::colvec _parent_data_s;



public:
	arma::colvec data_eV() const { return _data_eV; }
	arma::colvec data_s() const { return _data_s; }


	double eval_at_eV(const double eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {
		arma::colvec eV_single = { eV };
		arma::colvec single = eval_at_eV(eV_single, interp_method);
		return single(0);
	};


	arma::colvec eval_at_eV(const arma::colvec& eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {
		
		arma::colvec temp = eV * 0; // correct size
	
		if (this->code() == lib::CollisionCode::elastic ||
			this->code() == lib::CollisionCode::effective ||
			this->code() == lib::CollisionCode::attachment) {

			lib::grid_s_as_continuous(this->_data_eV, this->_data_s, eV, temp, interp_method);
		}
		else if (this->code() == lib::CollisionCode::excitation ||
			this->code() == lib::CollisionCode::ionization) {
			lib::grid_s_as_thresholded(this->_data_eV, this->_data_s, eV, temp, this->_eV_thresh, interp_method);
		}
		else if (this->code() == lib::CollisionCode::superelastic) {
			temp = lib::interp_sup_from_exc(this->_parent_data_eV, this->_parent_data_s, this->_parent_eV_thresh, eV, this->_g, interp_method);
		}

		return temp * _scale;
	};
	

	void print() {
		std::cout << _process << std::endl;
		
		switch (this->code()) {
		
		case lib::CollisionCode::elastic: std::cout << "mratio: " << this->Mratio() << std::endl; break;
		case lib::CollisionCode::effective: std::cout << "mratio: " << this->Mratio() << std::endl; break;
		case lib::CollisionCode::excitation: std::cout << "E: " << this->eV_thresh() << std::endl; break;
		case lib::CollisionCode::superelastic: std::cout << "parent_E: " << this->parent_eV_thresh() << "\t g: " << this->g() << std::endl; break;
		case lib::CollisionCode::ionization: std::cout << "E: " << this->eV_thresh() << std::endl; break;
		}

		arma::mat temp = arma::mat(_data_eV.n_rows, 2);
		temp.col(0) = _data_eV;
		temp.col(1) = _data_s;
		temp.print();
	}
};





class DiscreteTotalAttachment : public lib::DiscreteXsec {

public:

	DiscreteTotalAttachment(const arma::colvec& data_eV, const arma::colvec& data_s, const std::string& reactant, const std::string& product, const std::string& process) {
		
		this->_code = lib::CollisionCode::attachment;

		this->_reactant = reactant;
		this->_product = product;
		this->_process = process;

		this->_data_eV = data_eV;
		this->_data_s = data_s;
	}

};




class DiscreteElasticMomentumTransfer : public lib::DiscreteXsec {
public:

	DiscreteElasticMomentumTransfer(const arma::colvec& data_eV, const arma::colvec& data_s, const double Mratio, const std::string& reactant, const std::string& product, const std::string& process) {
		
		this->_code = lib::CollisionCode::elastic;
		
		this->_reactant = reactant;
		this->_product = product;
		this->_process = process;
		
		this->_data_eV = data_eV;
		this->_data_s = data_s;
		this->_Mratio = Mratio;

	}

};

class DiscreteEffective : public lib::DiscreteXsec {
public:

	DiscreteEffective(const arma::colvec& data_eV, const arma::colvec& data_s, const double Mratio, const std::string& reactant, const std::string& product, const std::string& process) {
		
		this->_code = lib::CollisionCode::effective;
		
		this->_reactant = reactant;
		this->_product = product;
		this->_process = process;
		
		this->_data_eV = data_eV;
		this->_data_s = data_s;
		this->_Mratio = Mratio;
	}


};

class DiscreteTotalExcitation : public lib::DiscreteXsec {
public:


	DiscreteTotalExcitation(const arma::colvec& data_eV, const arma::colvec& data_s, const double eV_thresh, const std::string& reactant, const std::string& product, const std::string& process) {
		
		this->_code = lib::CollisionCode::excitation;

		this->_reactant = reactant;
		this->_product = product;
		this->_process = process;
		
		this->_data_eV = data_eV;
		this->_data_s = data_s;
		this->_eV_thresh = eV_thresh;
	}


};

class DiscreteTotalIonization : public lib::DiscreteXsec {
public:

	DiscreteTotalIonization(const arma::colvec& data_eV, const arma::colvec& data_s, const double eV_thresh, const std::string& reactant, const std::string& product, const std::string& process) {
		
		this->_code = lib::CollisionCode::ionization;
		
		this->_reactant = reactant;
		this->_product = product;
		this->_process = process;
		
		this->_data_eV = data_eV;
		this->_data_s = data_s;
		this->_eV_thresh = eV_thresh;
	}


};






class DerivedTotalSuperelastic : public lib::DiscreteXsec {

	//std::shared_ptr<lib::DiscreteTotalExcitation> _parent_exc; // only useful for superelastic

public:

	DerivedTotalSuperelastic (std::shared_ptr<lib::DiscreteTotalExcitation> parent_exc, const double g) {
		
		this->_code = lib::CollisionCode::superelastic;
		
		this->_g = g;

		//this->_parent_exc = parent_exc; // Ref Count Should be Inc'd by pass by value

		this->_parent_data_eV = parent_exc->data_eV(); // the data, in this case, is actually the same as the parent exc.
		this->_parent_data_s = parent_exc->data_s();

		this->_parent_eV_thresh = parent_exc->eV_thresh();

		this->_info =  parent_exc->info();

		
		this->_reactant = parent_exc->product();
		this->_product = parent_exc->reactant();
		this->_process = "E + " + _reactant + " -> E + " + _product + ", Superelastic (derived)";

	}

};

#endif


