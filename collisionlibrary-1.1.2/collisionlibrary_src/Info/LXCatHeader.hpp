// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_LXCATHEADER
#define COLLISIONLIBRARY_LXCATHEADER


#include "collisionlibrary"


// The most meaningful info to ascribe to a Xsec defined by an LXCat data file
//class LXCatHeader : public lib::AbstractInfo {
//
//private:
//
//	int temporary = 0;
//
//	std::string _parent_file;
//
//	std::vector<std::string> _raw_header;
//
//	std::string _date_retrieved;
//	std::string _database;
//
//public:
//
//	// one caveat of LXCat headers: the database, and date-origin of info cannot be found locally to the collision
//	// these elements should be found while reading the file instead, or hard-set
//	LXCatHeader(std::vector<std::string> raw_header, std::string database, std::string date_retrieved, std::string parent_file) {
//		this->_raw_header = _raw_header;
//		this->_parent_file = parent_file;
//		this->_database = database;
//		this->_date_retrieved = date_retrieved;
//
//		this->_reference = _database + " database, www.lxcat.net, retrieved on " + _date_retrieved ;
//
//	}
//
//	std::string database() { return _database; };
//	std::string date_retrieved() { return _date_retrieved; }
//	std::string parent_file() { return _parent_file; }
//
//	
//
//	void print() {
//		std::cout << "LXCatHeader with:" << std::endl;
//		std::cout << "Database: " << database() << std::endl;
//		std::cout << "From file: " << parent_file() << std::endl;
//		std::cout << "Retrieved on: " << date_retrieved() << std::endl;
//		std::cout << "Reference: " << reference() << std::endl;
//	}
//
//
//
//};

#endif