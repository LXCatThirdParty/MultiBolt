// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_IDENTIFYSPECIES
#define COLLISIONLIBRARY_IDENTIFYSPECIES

#include "collisionlibrary"


// Get a list of strings of the species which are described
// as either products or reactants in the file
std::vector<std::string>
	identify_species_in_LXCat_file(const std::string& Xsec_fid) {

	using std::vector;
	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;

	using std::string;


	std::vector<std::string> found_species;


	ifstream fin; // file reading stream
	stringstream ss; // message output stream
	fs::path fpath = Xsec_fid;

	// fresh-start from file beginning
	fin.open(fpath.c_str());


	if (!fin) { // if the file can't be opened
		lib::file_not_opened_err(Xsec_fid);
	}

	string line = ""; // placeholder, filled while reading


	while (getline(fin, line)) {

		// in this case, product and reactant are the same, and only one appears
		if (lib::same_string(line, lib::ELASTIC_STRING) || lib::same_string(line, lib::EFFECTIVE_STRING)) {

			getline(fin, line);

			found_species.push_back({ lib::trim_copy(line) });
			

		}
		else if (lib::same_string(line, lib::EXCITATION_STRING)) {
			// account for how the double arrow might exist

			getline(fin, line);

			std::vector<std::string> bodies;
			std::string delimiter;

			if (line.find("<->", 0) != std::string::npos) { // if double arrow is  somewhere
				delimiter = "<->";
			}
			else { // if double arrow is nowhere
				delimiter = "->";
			}


			bodies = lib::split(line, delimiter);
			found_species.push_back({ lib::trim_copy(bodies[0])}); // take first body, the reactant
			found_species.push_back({ lib::trim_copy(bodies[1])}); // also add in the product for possible superelastic identification

		}
		else if (lib::same_string(line, lib::ROTATIONAL_STRING) || lib::same_string(line, lib::VIBRATIONAL_STRING)) { // contingency for rotvib format from database
			// the usual excitation header lines do not exist for some reason!
			// have to back into it via the PROCESS string.

			std::vector<std::string> raw_header = {};
			raw_header.push_back(line);
			while (line.find("COLUMNS: Energy (eV) | Cross section (m2)") == std::string::npos) {
				getline(fin, line);
				raw_header.push_back(line);
			}

			auto bodies = find_rotvib_species_from_raw_header(raw_header);

			found_species.push_back(bodies.at(0));
			found_species.push_back(bodies.at(1));
			
		}
		else if (lib::same_string(line, lib::IONIZATION_STRING) ||
			lib::same_string(line, lib::ATTACHMENT_STRING)) {

			// line-chuck once to look at next line
			getline(fin, line);

			std::string delimiter;
			if (line.find("<->", 0) != std::string::npos) { // if double arrow is  somewhere
				delimiter = "<->";
			}
			else { // if double arrow is nowhere
				delimiter = "->";
			}

			std::vector<std::string> bodies = lib::split(line, delimiter);
			found_species.push_back({ lib::trim_copy(bodies[0]) }); // take first body, the reactant
			found_species.push_back({ lib::trim_copy(bodies[1]) }); // also add in the ion. TODO: relevant to future ion effects?

		}
		else {
			continue; // pass on
		}
	}

	fin.close();



	
	std::vector<std::string> found_species_new;

	std::set<int> rep_idx;
	for (auto it = found_species.begin(); it != found_species.end(); it++) {

		auto name = *it;

		int count = 0;
		int idx = 0;
		for (auto it2 = found_species.begin(); it2 != found_species.end(); it2++) {

			
			if (lib::same_string(name, *it2)) {
				count++;
				if (count > 1) {
					rep_idx.insert(idx);
				}
			}
			
			idx++;

		}


	}

	// populate new species
	for (int idx = 0; idx < found_species.size(); ++idx) {
			
		// if current index is not in set
		if (rep_idx.count(idx) == 0) {
			found_species_new.push_back(found_species[idx]);
		}
	}


	return found_species_new;
}

#endif