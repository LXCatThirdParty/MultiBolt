// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_SPECIES
#define COLLISIONLIBRARY_SPECIES
#include "collisionlibrary"


// A container of cross-sections which involve the same reactant
// Electrons which collide with neutrals of the same named as the
// reactant undergo the collisions held within.
class Species {



public:

	

	
	typedef std::vector< std::shared_ptr<lib::AbstractXsec>> XsecPtrVec;

	XsecPtrVec allcollisions, att, ela, eff, exc, iz, sup = {};
	


	std::string _name; // body name

	Species(std::string name) {
		this->_name = name;
	}

	void name(std::string name) {
		this->_name = name;
	}

	std::string name() {
		return _name;
	}



	double _frac = 0;


	Species() {};
	Species(std::shared_ptr<lib::AbstractXsec> x) { add_Xsec(x); };
	Species(std::vector<std::shared_ptr<lib::AbstractXsec>> vec) { 
		for (auto x : vec) {
			add_Xsec(x);
		}
	};


	double frac() { return _frac; }

	auto n_all() { return allcollisions.size(); }
	auto n_att() { return att.size(); }
	auto n_ela() { return ela.size(); }
	auto n_eff() { return eff.size(); }
	auto n_exc() { return exc.size(); }
	auto n_iz() { return iz.size(); }
	auto n_sup() { return sup.size(); }

	


	XsecPtrVec* pick_from_code(lib::CollisionCode c) {
		switch (c) {
			case lib::CollisionCode::attachment:
				return &att;
			case lib::CollisionCode::elastic:
				return &ela;
			case lib::CollisionCode::effective:
				return &eff;
			case lib::CollisionCode::excitation:
				return &exc;
			case lib::CollisionCode::rotational: ///// ----!----
				return &exc;
			case lib::CollisionCode::ionization:
				return &iz;
			case lib::CollisionCode::superelastic:
				return &sup;
			default:
				lib::bad_collision_code();
		}

		return nullptr;
	}


	void add_Xsec(std::shared_ptr<lib::AbstractXsec> x);

	
	// allow you to erase simulataneously from all and the specific collision array
	std::vector<std::shared_ptr<lib::AbstractXsec>>::iterator
		erase_safely_from_all(std::vector<std::shared_ptr<lib::AbstractXsec>>::iterator it);

	void erase_by_process(std::string process);
	void erase_by_index(int k);
	void erase_by_index(lib::CollisionCode c, int k);


	void assign_scale_by_process(std::string process, double s);
	void assign_scale_by_index(int k, double s);
	void assign_scale_by_index(lib::CollisionCode c, int k, double s);

	void assign_scattering_by_type(lib::CollisionCode c, lib::Scattering scattering);
	void assign_scattering_by_index(int k, lib::Scattering scattering);
	void assign_scattering_by_index(lib::CollisionCode c, int k, lib::Scattering scattering);
	void assign_scattering_by_process(std::string process, lib::Scattering scattering);


	void print() {

		std::stringstream ss; ss.str("");
		ss.setf(std::ios_base::scientific, std::ios_base::floatfield);

		ss << "\n";
		ss << "Species: " << _name << "\n";
		ss << "Fraction: " << std::setprecision(4) << _frac << "\n";
		ss << "N_all: " << n_all() << "\n";
		ss << "N_att: " << n_att() << "\n";
		ss << "N_ela: " << n_ela() << "\n";
		ss << "N_eff: " << n_eff() << "\n";
		ss << "N_exc: " << n_exc() << "\n";
		ss << "N_iz: " << n_iz() << "\n";
		ss << "N_sup: " << n_sup() << "\n";

		std::cout << ss.str() << std::endl;

		for (auto x : allcollisions) {
			x->print();
		}


	};

	void clear() {
		allcollisions = {};
		att = {};
		ela = {};
		eff = {};
		exc = {};
		iz = {};
		sup = {};
	}

	bool is_valid() {

		// are there any collisions that are not inside allcollisions? or any unsorted?
		if (n_all() != (n_att() + n_ela() + n_eff() + n_exc() + n_iz() + n_sup())) {

			lib::normal_statement("Species is invalid because individual collision vectors do not add up to the size of 'all'.");
			return false; // invalid if any collisions are unsorted or absent from 'allcollisions'.
		}

		std::set<std::string> processes;

		for (auto x : allcollisions) {

			if (x->code() == lib::CollisionCode::nocollision) {
				lib::normal_statement("Species is invalid because some collisions are defined as 'nocollision'.");
				return false;// invalid if any collisions are 'nocollisions'.
			}

			processes.insert(x->process());
		}

		if (processes.size() != allcollisions.size()) {
			lib::normal_statement("Species is invalid because some collisions are duplicates.");
			return false; // invalid if any processes are duplicates
		}


		auto codevec = { lib::CollisionCode::attachment,
							lib::CollisionCode::elastic,
							lib::CollisionCode::effective,
							lib::CollisionCode::excitation,
							lib::CollisionCode::ionization,
							lib::CollisionCode::superelastic };

		for (auto c : codevec) {

			auto ptr = pick_from_code(c);

			for (auto x : *ptr) {

				if (x->code() != c) {
					lib::normal_statement("Species is invalid because some collisions are in the wrong container for their code.");
					return false; // invalid if any collisions are in a bucket they are not supposed to be.
				}

			}

		}


		return true;
	}

	bool has_process(const std::string& process) {

		for (auto x : allcollisions) {

			if (lib::same_string(x->process(), process)) {
				return true;
			}

		}

		return false;
	}

	std::shared_ptr<lib::AbstractXsec> get_process(const std::string& name) {

		if (!has_process(name)) {

			lib::collision_not_found(name);

			return nullptr;
		}


		for (auto it = allcollisions.begin(); it != allcollisions.end(); it++) {

			if (lib::same_string(name, (*it)->process())) {
				return (*it);
			}
		}

	}
};

#endif
