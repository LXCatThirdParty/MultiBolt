// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_SPECIESADD
#define COLLISIONLIBRARY_SPECIESADD
#include "collisionlibrary"

// you are expected to make_shared on your own, and pass in the shared_ptr
void lib::Species::add_Xsec(std::shared_ptr<lib::AbstractXsec> x) {

	// do not add Xsec if it is not fully defined!
	if (x->code() == lib::CollisionCode::nocollision) {
		lib::normal_statement("Xsec with process [" + x->process() + " was not added because it was defined as 'nocollision'.");
		return;
	}


	allcollisions.push_back(x);

	lib::CollisionCode c = x->code();

	(*pick_from_code(c)).push_back(x);

	return;
}

#endif





