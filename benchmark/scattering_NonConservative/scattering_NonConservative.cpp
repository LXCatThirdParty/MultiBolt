// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// 09/19/2022
// Calculates some Lucas-Salee style benchmarks for various analytic scattering treatments

#include "multibolt"



int main() {


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();



	
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE;
	p.N_terms = 10; 

	p.Nu = 1000;	
	p.weight_f0 = 1.0;	// Iteration weight. 


	p.initial_eV_max = 50;
	p.USE_ENERGY_REMAP = true;		// Look for a new grid if necessary
	p.remap_grid_trial_max = 25;				// try a new grid no more than this many times
	p.remap_target_order_span = 15.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)

	p.EN_Td = 10;
	p.p_Torr = 760;
	p.T_K = 0;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization


	std::vector<lib::Scattering> vec_ela_scattering = 
		{ lib::isotropic_scattering() , lib::isotropic_scattering() , lib::isotropic_scattering() , lib::screened_coulomb_scattering() };
	std::vector<lib::Scattering> vec_inel_scattering =
	{ lib::isotropic_scattering() , lib::forward_scattering() , lib::screened_coulomb_scattering() , lib::screened_coulomb_scattering() };


	/* Perform Boltzmann solver calculation  */

	// instantiate
	mb::BoltzmannSolver run;
	mb::BoltzmannOutput out;

	

	mb::normal_statement("Using following parameters as starting-point for validation:");
	p.print();


	std::vector<std::vector<double>> args = { {0, 0, 0}, { 0.25, 0,0 }, { 0.5, 0,0 }, { 0.75, 0,0 } , { 1.0,0,0 }, 
		{0, 5e-4, 0.5}, {0, 2e-3, -0.5}, {0, 8e-3,-1.0 } };

	std::vector < std::string > args_strings = { "F=0, a=0, p=0", "F=0.25, a=0, p=0", "F=0.5, a=0, p=0", "F=0.75, a=0, p=0", "F=1.0, a=0, p=0", 
		"F=0, a=5e-4, p=0.5", "F=0, a=2e-3, p=-0.5", "F=0, a=8e-3, p=-1.0" };



	std::vector<std::string> args_scatter_strings = { "iso-iso", "iso-fwd", "iso-sc", "sc-sc" };

	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

	// vectors of ouputs:
	std::vector<std::vector<mb::BoltzmannOutput>> LS_scatter_sols(args.size(), std::vector<mb::BoltzmannOutput>(vec_ela_scattering.size(), mb::BoltzmannOutput()));


	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < args.size(); ++i) {
	
		/* Develop library of analytic species -------------------------------------------------------- */

		double F = args.at(i).at(0);
		double a = args.at(i).at(1);
		double rho = args.at(i).at(2);

		lib::Library this_LS = lib::get_LucasSalee_Library(F, a, rho);

		for (int j = 0; j < vec_ela_scattering.size(); ++j) {

			mb::BoltzmannParameters this_p = p;

			this_LS.assign_scattering_by_type(vec_ela_scattering.at(j), lib::CollisionCode::elastic);
			this_LS.assign_scattering_by_type(vec_inel_scattering.at(j), lib::CollisionCode::excitation);
			this_LS.assign_scattering_by_type(vec_inel_scattering.at(j), lib::CollisionCode::ionization);


			mb::normal_statement("Beginning validation solution: Lucas-Salee : " + args_scatter_strings.at(j));
			run = mb::BoltzmannSolver(this_p, this_LS);
			LS_scatter_sols.at(i).at(j) = run.get_output();

		}
	
	}
	



	for (int i = 0; i < args.size(); ++i) {

		printf("\n%s", args_strings.at(i).c_str());

		for (int j = 0; j < vec_ela_scattering.size(); ++j) {

			printf("\n\t%s", args_scatter_strings.at(j).c_str());

			
			printf("\n\t\tavg_en :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).avg_en);
			printf("\n\t\tk_iz_eff_N :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).k_iz_eff_N);
			printf("\n\t\tW_FLUX :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).W_FLUX);
			printf("\n\t\tW_BULK :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).W_BULK);
			printf("\n\t\tDTN_FLUX :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).DTN_FLUX);
			printf("\n\t\tDTN_BULK :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).DTN_BULK);
			printf("\n\t\tDLN_FLUX :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).DLN_FLUX);
			printf("\n\t\tDLN_BULK :\t\tMB: %1.4e", LS_scatter_sols.at(i).at(j).DLN_BULK);

			std::cout << std::endl;
		}
	}



	return 0;
}

