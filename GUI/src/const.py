import configparser
import os as os
from os import path



#create your own color pallete of hex values to draw from
#colors = ("#99b433","#00a300","#1e7145","#ff0097","#9f00a7","#7e3878","#603cba","#00aba9","#2d89ef","#2b5797","#ffc40d","#e3a21a","#da532c","#ee1111","#b91d47",)


ini_name = "config.ini"


# check that config.ini actually exists
if not os.path.exists("config.ini"):
    print("Notice: config.ini was not found. Because this is necessary to run, a barebones config.ini will be printed. Be sure to check all paths before you continue.")
    str = """[Default]
default_Xsec_location : ../../cross-sections/

default_export_location : ../GUI_Exported_MultiBolt_Data/

default_exe_location : ../../bin/multibolt_win64.exe

N_terms : 6

model : HD+GE

Nu : 200

p_Torr : 760

T_K : 300

EN_Td : 100

LXCat_Xsec_fid : Biagi_N2.txt

species : N2 1.0

sweep_option : EN_Td

sweep_style : lin
	100
	500
	5

export_name : 

NO_EXPORT : No

EXPORT_XSECS : No

conv_err : 1e-4

iter_max : 100

iter_min : 4

initial_eV_max : 100

weight_f0 : 0.8

USE_ENERGY_REMAP : Yes

remap_target_order_span : 10

remap_grid_trial_max : 10

remap_allowance : 5

USE_EV_MAX_GUESS : Yes

elastic_scattering : Isotropic

excitation_scattering : Isotropic

ionization_scattering : Isotropic

superelastic_scattering : Isotropic

sharing : 0.5

multibolt_num_threads : max

SILENT : No

SHY : No

DONT_ENFORCE_SUM : No

interp_method : Linear

USE_NU_REMAP : No

remap_Nu_max : 2000

remap_Nu_increment : 200
"""
    with open(ini_name, 'w') as f:
        f.write(str)
    pass



config = configparser.ConfigParser()
config.read(ini_name)
default = config["Default"]

#global DEFAULT_EXPORT_PATH
HOME = os.getcwd()
EXPORT_PATH  = default["default_export_location"]
XSEC_PATH  = default["default_Xsec_location"]
EXE_PATH = default["default_exe_location"]

# special colors # Note: colors are not colorblind-safe. Investigate a better pallete
# use link as tool: https://davidmathlogic.com/colorblind/
RED_COLOR = "#ff6666" # this is a red color
#STARTCOLOR = "#84e184" # this is a green color

# IBM PALETTE
START_COLOR = "#648FFF" # this is a blueish color
WARN_COLOR = "#FFB000" # this is a yellowish color

CRIT_COLOR = "#FE6100" # this is an orangeish color

START_POS_FRAC = 0.05
START_SIZE_FRAC = 0.75;


stylesheet_str = "QWidget{font-size: 10pt;}"

#LARGE_BTN_SIZE = QSize(100, 40)
#NORMAL_LINE_SIZE = QSize(60,20)
#NORMAL_BTN_SIZE = QSize(100, 25)


if __name__ == "__main__":
    pass