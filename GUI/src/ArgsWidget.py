import configparser
import os

from PyQt5.QtWidgets import QApplication, QPushButton, QWidget, QMainWindow, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QComboBox, QTextEdit, QButtonGroup, QVBoxLayout, QCheckBox, QFormLayout, QTabWidget, QRadioButton
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont

class ArgsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.txt = QTextEdit(self)
        self.txt.setReadOnly(True)

        #font = QFont(self)
        #font.setPointSize(24)
        #self.txt.setFontPointSize(12)


        self.writeButton = QPushButton("Write Arguments", self)
        self.clearButton = QPushButton("Clear Arguments", self)
        pass

    def connects(self):
        self.clearButton.clicked.connect(lambda: self.txt.setText(""))
        pass
    
    def layout(self):
        layout = QVBoxLayout(self)
        layout.addWidget(self.txt)
        layout.addWidget(self.writeButton)
        layout.addWidget(self.clearButton)
        self.setLayout(layout)
        pass

if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = ArgsWidget(window)

    x.show()


    sys.exit(app.exec_())