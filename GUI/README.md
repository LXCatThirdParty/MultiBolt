# Use

MultiBoltGUI is essentially a wrapper for command-line arguments to pass to a MultiBolt binary.

Before you start, open config.ini and check the paths. Make sure a MultiBolt binary exists at the *default_exe_location*, and edit this path if need-be.

MultiBoltGUI uses *config.ini* as a default startup file. This means you must execute MultiBoltGUI in the same directory as *config.ini*




# Install

MultiBoltGUI is a PyQt5 application compiled using PyInstaller v3.6.


MultiBoltGUI may be run using just Python 3. You can make sure you have all dependencies listed in "requirements.txt" by doing:

> cd src

> pip install -r requirements.txt
	
Then, still in the source directory, using this command to start the GUI (make sure you have python 3.7 installed and recognizable in your PATH):

> python MultiBoltGUI.py


# Build

To compile your own binary of the GUI, use the following:

* If you do not already have pyinstaller, call this to install it:

> pip install pyinstaller

	
* Once pyinstaller is present, call:

> cd src

> pyinstaller --onefile MultiBoltGUI.py

	
* The exectuable will be placed in *GUI/src/dist*.
* You must execute it in the same directory as 'config.ini', so please move it to where you like.









