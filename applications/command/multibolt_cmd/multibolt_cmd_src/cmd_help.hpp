// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt_cmd"


void cmd_help() {
	using std::cout;
	using std::endl;


	cout << "\nDisplaying line options for MultiBolt v_3" <<

		"\n\n\t--help\t\t\t\tDisplay command line options." <<

		"\n\n\t*** Basic Parameters ***" <<
		"\n\n\t--model\t\t\tDefine model in use." <<
		"\n\t\t\t\t\tType 1 (Hydrodynamic): The next arg must be HD" <<
		"\n\t\t\t\t\tex: --model\tHD" <<
		"\n\t\t\t\t\tType 2 (Hydrodynamic plus density gradient expansion): The next arg must be HD+GE" <<
		"\n\t\t\t\t\tex: --model\tHD+GE" <<
		"\n\t\t\t\t\t    (to solve only up to order _1L and _1T, use HD+GE_01 instead)"
		"\n\t\t\t\t\tType 3 (Steady-State Townsend): The next arg must be SST" <<
		"\n\t\t\t\t\tex: --model\tSST" <<

		"\n\n\t--N_terms\t\t\tDefine number of terms to expand over." <<
		"\n\t\t\t\t\tThe next arg must be [int]" <<
		"\n\t\t\t\t\tex: --N_terms\t6" <<

		"\n\n\t--Nu\t\t\t\tDefine energy points in the finite difference scheme." <<
		"\n\t\t\t\t\tThe next arg must be [int]" <<
		"\n\t\t\t\t\tex: --Nu\t1000" <<

		"\n\n\t--p_Torr\t\t\t\tDefine the environmental pressure (in Torr)." <<
		"\n\t\t\t\t\tThe next arg must be [float]" <<
		"\n\t\t\t\t\tex: --p_Torr\t760" <<

		"\n\n\t--T_K\t\t\t\tDefine the environmental temperature (in Kelvin)." <<
		"\n\t\t\t\t\tThe next arg must be [float]" <<
		"\n\t\t\t\t\tex: --T_K\t300" <<
		
		"\n\n\t--EN_Td\t\t\t\tDefine the external electric field (in Td)." <<
		"\n\t\t\t\t\tThe next arg must be [float]" <<
		"\n\t\t\t\t\tex: --EN_Td\t100" <<
		
		
		
		
		"\n\n\t*** cross-section Input ***" <<
		
		"\n\n\t--LXCat_Xsec_fid\t\t\t\tDefine one LXCat cross-section to read." <<
		"\n\t\t\t\t\tThe next arg must be [Xsec File Path = string]" <<
		"\n\t\t\t\t\tex: --LXCat_Xsec_fid\t\"../Cross-Sections/Biagi_Ar.txt\"" <<
		
		"\n\n\t--species\t\t\t\tDefine one species to use out of the files provided, and its fractional prevalence." <<
		"\n\t\t\t\t\tSpecies can only be used if they can be identified explicitly in the files provided as either reactants or products." <<
		"\n\t\t\t\t\tThe next args must be [Name = \"string\"] [float (/1.0)]" <<
		"\n\t\t\t\t\tex: --species\t\"Ar\"\t1.00" <<
		
		"\n\n\t--KEEP_ALL_SPECIES\t\t\tDenote that you prefer to keep all discovered species regardless of presence." <<
		"\n\t\t\t\t\tBy default, only species which are asked-for will be saved." <<
		
		"\n\n\t--scale_Xsec\t\t\tDefine a scalar weight (not the fractional dependence) to apply to a particular collision." <<
		"\n\t\t\t\t\tThe next args must be [Process = \"string\"] [float]" <<
		"\n\t\t\t\t\tex: --scale_Xsec\t\"E + Ar <-> E + Ar, Elastic\"\t1.00" <<

		
		"\n\n\t*** Sweep BoltzmannParameters ***" <<
		"\n\n\tThe following are optional: if unset, a single run (vs EN_Td) is assumed." <<
		
		
		"\n\n\t--sweep_option\t\tDefine the variable you are interested in sweeping over." <<
		"\n\t\t\t\t\tType 1 (electric field strength): The next arg must be EN_Td" <<
		"\n\t\t\t\t\tex: --sweep_option\t EN_Td" <<
		"\n\t\t\t\t\tType 2 (temperature): The next arg must be T_K" <<
		"\n\t\t\t\t\tex: --sweep_option\t T_K" <<
		"\n\t\t\t\t\tType 3 (pressure): The next arg must be p_Torr" <<
		"\n\t\t\t\t\tex: --sweep_option\t p_Torr" <<
		"\n\t\t\t\t\tType 4 (binary-fraction): The next arg must be bin_frac. " <<
		"\n\t\t\t\t\tThe *first* species will be assigned the fractional prevalence, with the *second* species assigned [1.0 - frac]. " <<
		"\n\t\t\t\t\tAll other species will be removed." <<
		"\n\t\t\t\t\tex: --sweep_option\t bin_frac" <<
		"\n\t\t\t\t\tType 5 (grid points): The next arg must be Nu" <<
		"\n\t\t\t\t\tex: --sweep_option\t Nu" <<
		"\n\t\t\t\t\tType 6 (expansion terms): The next arg must be N_terms" <<
		"\n\t\t\t\t\tex: --sweep_option\t N_terms" <<
		


		"\n\n\t--sweep_style\t\tDefine the space style and values to sweep over." <<
		"\n\t\t\t\t\tThe values will be assigned to the parameter denoted by sweep_option." <<
		"\n\t\t\t\t\tIf the defined sweep_option is an integer value (ex: N_terms), the value will be typecast and possibly truncated." <<
		"\n\t\t\t\t\tType 1 (linear): The next args must be lin followed by [start = float] [end = float] [points = int]" <<
		"\n\t\t\t\t\tex: --sweep_style\tlin\t100\t500\t5" <<
		"\n\t\t\t\t\tType 2 (logarithmic): The next args must be log followed by [log10(start) = float] [log10(end) = float] [points = int]" <<
		"\n\t\t\t\t\tex: --sweep_style\tlog\t0\t3\t10" <<
		"\n\t\t\t\t\tType 3 (regular): The next args must be reg followed by [start = float] [delta = float] [end = float]" <<
		"\n\t\t\t\t\tex: --sweep_style\treg\t100\t100\t500" <<
		"\n\t\t\t\t\tType 4 (defined): The next args must be def followed by any number of floats of your choice." <<
		"\n\t\t\t\t\tex: --sweep_style\tdef\t100\t200\t300\t400\t500" <<
		
		
"\n\n\t*** Export BoltzmannParameters ***" <<

"\n\n\t--LIMIT_EXPORT\t\tDenote that only k_iz_eff_N, W_BULK, DLN_BULK, and alpha_eff_N will be written. (will be superceded by --NO_EXPORT, if present)." <<

"\n\n\t--export_location\t\tDefine export location directory." <<
"\n\t\t\t\t\tThe next arg must be [path = \"string\"]" <<
"\n\t\t\t\t\tex: --export_location\t\"../Exported_Multibolt_Data\"" <<

"\n\n\t--export_name\t\tDefine name of this export (to be placed inside export_location)." <<
"\n\t\t\t\t\tBy default, runs are named Run_0, Run_1, etc." <<
"\n\t\t\t\t\tThe next arg must be [path = \"string\"]" <<
"\n\t\t\t\t\tex: --export_name\t\"../My_MultiBolt_Run\"" <<

"\n\n\t*** Scattering Options ***" <<

"\n\n\t--elastic_scattering\t\t\tDefine scattering model for elastic (or, 'effective') collisions." <<
"\n\t\t\t\t\tChoose one of: [Isotropic, ScreenedCoulomb] (note: IdealForward is non-physical)" <<

"\n\n\t--excitation_scattering\t\t\tDefine scattering model for excitation collisions." <<
"\n\t\t\t\t\tChoose one of: [Isotropic, IdealForward, ScreenedCoulomb]" <<

"\n\n\t--ionization_scattering\t\t\tDefine scattering model for ionization collisions." <<
"\n\t\t\t\t\tChoose one of: [Isotropic, IdealForward, ScreenedCoulomb]" <<

"\n\n\t--superelastic_scattering\t\t\tDefine scattering model for superelastic collisions." <<
"\n\t\t\t\t\tChoose one of: [Isotropic, IdealForward, ScreenedCoulomb]" <<

"\n\n\t--sharing\t\t\tDefine the energy sharing between primary and secondary electron during ionization." <<
"\n\t\t\t\t\tThe next arg must be [float]. Use in range (0, 0.5]" <<
"\n\t\t\t\t\tex: --sharing\t\0.5" <<

"\n\n\t*** Advanced BoltzmannParameters ***" <<



"\n\n\t--conv_err\t\t\t\tDefine convergence error threshold for iterative schemes." <<
"\n\t\t\t\t\tThe next arg must be [float]" <<
"\n\t\t\t\t\tex: --conv_err\t1e-4" <<

"\n\n\t--iter_max\t\t\tDefine maximum number of iterations." <<
"\n\t\t\t\t\tThe next arg must be [int]" <<
"\n\t\t\t\t\tex: --iter_max\t100" <<

"\n\n\t--iter_min\t\t\tDefine minimum iterations." <<
"\n\t\t\t\t\tThe next arg must be [int]" <<
"\n\t\t\t\t\tex: --iter_min\t10" <<

"\n\n\t--initial_eV_max\t\t\t\tDefine eV_max (in eV)." <<
"\n\t\t\t\t\tThe actual value of eV_max may change if USE_ENERGY_REMAP." <<
"\n\t\t\t\t\tThe next arg must be [float]" <<
"\n\t\t\t\t\tex: --initial_eV_max\t100" <<

"\n\n\t--USE_EV_MAX_GUESS\t\t\t\tIndicate that you'll let MultiBolt guess the initial energy grid on its own. Allowing remap is reccomended." <<
"\n\t\t\t\t\tex: --USE_EV_MAX_GUESS" <<

"\n\n\t--weight_f0\t\t\t\tDefine the weight in the SOR scheme for f0." <<
"\n\t\t\t\t\tThis can sometimes help convergence." <<
"\n\t\t\t\t\tThe next arg must be [float]" <<
"\n\t\t\t\t\tex: --weight_f0\t0.8" <<


"\n\n\t*** 'Remap'-related Advanced BoltzmannParameters ***" <<

"\n\n\t--USE_ENERGY_REMAP\t\t\tDenote using USE_ENERGY_REMAP. After solving f0, MultiBolt will attempt to find a best-fit of grid maximum." <<
"\n\t\t\t\t\tex: --USE_ENERGY_REMAP" <<

"\n\n\t--remap_target_order_span\t\t\t\tDefine the preferred log-height of the EEDF." <<
"\n\t\t\t\t\tIf USE_ENERGY_REMAP is activated, the energy grid will be adjusted to attempt to make this condition true." <<
"\n\t\t\t\t\tThe next arg must be [float]" <<
"\n\t\t\t\t\tex: --remap_target_order_span\t10" <<

"\n\n\t--remap_grid_trial_max\t\t\t\tDefine maximum number of attempts to devote to USE_ENERGY_REMAP." <<
"\n\t\t\t\t\tThe next arg must be [int]" <<
"\n\t\t\t\t\tex: --remap_grid_trial_max\t10" <<

"\n\n\t--remap_allowance\t\t\t\tDefine the amount of difference from the 'target' decay that triggers remap decisions." <<
"\n\t\t\t\t\tAn EEDF with decay 'allowance'/2 larger/smaller than the target is considered worth remapping." <<
"\n\t\t\t\t\tThe next arg must be [float]" <<
"\n\t\t\t\t\tex: --remap_allowance\t10" <<

"\n\n\t--USE_NU_REMAP\t\t\tDenote using USE_NU_REMAP. After solving f0, MultiBolt will search f0 for negative elements (or a poor norm) and recalculate with a higher number of bins." <<
"\n\t\t\t\t\tex: --USE_NU_REMAP" <<

"\n\n\t--remap_Nu_max\t\t\t\tDefine maximum number of Nu for the remapping process in USE_NU_REMAP." <<
"\n\t\t\t\t\tThe next arg must be [int]" <<
"\n\t\t\t\t\tex: --remap_Nu_max\t2000" <<

"\n\n\t--remap_Nu_increment\t\t\t\tDefine amount by which Nu increases per iteration in USE_NU_REMAP." <<
"\n\t\t\t\t\tThe next arg must be [int]" <<
"\n\t\t\t\t\tex: --remap_Nu_increment\t200" <<


"\n\n\t*** Miscellaneous ***" <<

"\n\n\t--SILENT\t\t\tDenote that you do not want MultiBolt to print any statements." <<

// new as of 2/8/2022
"\n\n\t--multibolt_num_threads\t\t\tDefine maximum number of threads which MultiBolt will attempt to use, if available." <<
"\n\t\t\t\t\tMultithreading is generally used to save time as: " <<
"\n\t\t\t\t\t(1) to solve members of sweeps simultaneously and (2) to solve parallel orders of gradient descent" <<

// new as of 3/21/2022
"\n\n\t--EXPORT_XSECS\t\t\tExport cross sections which MultiBolt uses on a grid in the range [1e-3, 1e3]." <<
"\n\t\t\t\t\tThis can be useful for debugging purposes, to check the integrity of cross sections." <<

"\n\n\t--NO_EXPORT\t\tDenote that no data will be exported or written." <<

"\n\n\t--SHY\t\tDenote that non-realistic solutions will be terminated early, if possible." <<
"\n\t\t\t\t\tDo not use unless you are confident in your settings!" <<

// new as of 11/21/2022
"\n\n\t--DONT_ENFORCE_SUM\t\tAllow calculations to continue regardless of sum(species). Sometimes necessary for excited-state modeling." <<

// new as of 04/28/2023
"\n\n\t--interp_method\t\t\tDefine method for interpolating tabulated xsec data." <<
"\n\t\t\t\t\tChoose one of: [Linear, Logarithmic]" << std::endl;
}