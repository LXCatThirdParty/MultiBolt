// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

// Parse arguments passed in via command line

#pragma once

#include "multibolt_cmd"



void bad_arg(const std::string& arg) {
	mb::normal_statement("Could not start a calculation: bad argument [" + arg + "]. Try --help for details.");
	exit(-1);
}

// throws err if is not int
int enforce_is_int(const std::string& arg) {

	if (mb::isInt(arg)) {
		return std::stoi(arg);
	}
	else {
		bad_arg(arg);
	}

	return 0;
}

// throws err if is not float
double enforce_is_float(const std::string& arg) {
	if (mb::isFloat(arg)) {
		return std::stof(arg);
	}
	else {
		bad_arg(arg);
	}

	return 0.0;
}




std::tuple<mb::BoltzmannParameters, mb_cmd::LibraryParams, mb_cmd::SweepParams, mb_cmd::ExportParams, int> parse_args(const int argc, std::vector<std::string> argv) {
	using namespace mb;

	

	mb::BoltzmannParameters p;
	mb_cmd::SweepParams sweep_p;
	mb_cmd::LibraryParams lib_p;
	mb_cmd::ExportParams exp_p;

	int multibolt_num_threads = 1;
	

	

	
	bool SILENT = false; // if found, flip the global-detect to slience multibolt


	
		
	p.argslist = argv;


	mb::debug_statement("Parsing command line arguments: ");
	if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
		for (auto& s : p.argslist) {
			std::cout << s << std::endl;
		}
	}



	// You first need to parse for --help. if you do, ignore everything else
	for (auto& arg : argv) {
		if (mb::same_string(arg, "--help")) {
			mb_cmd::cmd_help(); // this will close the program inside help()
			exit(0);
		}
	}

	


	// first, look for  basic parameters (straight-forward to set)
	
	for (auto it = argv.begin(); it != argv.end(); it++) {

		auto this_idx = it - argv.begin();

		std::string arg;

		if (mb::same_string(*it, "--model")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "HD")) {
				p.model = mb::ModelCode::HD;
			}
			else if (mb::same_string(arg, "HD+GE")) {
				p.model = mb::ModelCode::HDGE;
			}
			else if (mb::same_string(arg, "HD+GE_01")) {
				p.model = mb::ModelCode::HDGE_01;
			}
			else if (mb::same_string(arg, "SST")) {
				p.model = mb::ModelCode::SST;
			}
			else {
				mb_cmd::bad_arg(arg);
			}

			it = it + 1;
			continue;
		}


		if (mb::same_string(*it, "--N_terms")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.N_terms = mb_cmd::enforce_is_int(arg);
		
			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--Nu")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.Nu = mb_cmd::enforce_is_int(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--p_Torr")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.p_Torr = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--T_K")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.T_K = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--EN_Td")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.EN_Td = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}


		// more advanced 
		if (mb::same_string(*it, "--conv_err")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.conv_err = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--iter_max")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.iter_max = mb_cmd::enforce_is_int(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--iter_min")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.iter_min = mb_cmd::enforce_is_int(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--initial_eV_max")) {
			arg = argv.at(this_idx + 1); // next 1 arg

			p.initial_eV_max = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--weight_f0")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.weight_f0 = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--USE_EV_MAX_GUESS")) {
			p.USE_EV_MAX_GUESS = true;
			continue;
		}


		// energy remap settings

		if (mb::same_string(*it, "--USE_ENERGY_REMAP")) {

			p.USE_ENERGY_REMAP = true;

			continue;
		}

		if (mb::same_string(*it, "--USE_NU_REMAP")) {

			p.USE_NU_REMAP = true;

			continue;
		}

		if (mb::same_string(*it, "--remap_Nu_max")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.remap_Nu_max = mb_cmd::enforce_is_int(arg);

			it = it + 1;
			continue;
		}
		
		if (mb::same_string(*it, "--remap_Nu_increment")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.remap_Nu_increment = mb_cmd::enforce_is_int(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--remap_target_order_span")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.remap_target_order_span = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--remap_allowance")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.remap_allowance = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--remap_grid_trial_max")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.remap_grid_trial_max = mb_cmd::enforce_is_int(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--sharing")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			p.sharing = mb_cmd::enforce_is_float(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--SHY")) {
			p.SHY = true;

			continue;
		}

		if (mb::same_string(*it, "--DONT_ENFORCE_SUM")) {
			p.DONT_ENFORCE_SUM = true;

			continue;
		}



		// cross section identification settings

		if (mb::same_string(*it, "--LXCat_Xsec_fid")) {
			arg = argv.at(this_idx + 1); // next 1 arg

			lib_p.LXCat_Xsec_fids.push_back(arg);

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--species")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			std::string arg2 = argv.at(this_idx + 2); // next 1 arg

			lib_p.species.push_back(arg);
			lib_p.fracs.push_back(mb_cmd::enforce_is_float(arg2));

			it = it + 2;
			continue;
		}

		if (mb::same_string(*it, "--KEEP_ALL_SPECIES")) {
			lib_p.KEEP_ALL_SPECIES = true;

			continue;
		}

		if (mb::same_string(*it, "--scale_Xsec")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			std::string arg2 = argv.at(this_idx + 2); // next 1 arg

			lib_p.processes.push_back(arg);
			lib_p.scales.push_back(mb_cmd::enforce_is_float(arg2)); //  todo: i sure hope the quotes actually work

			it = it + 2;
			continue;
		}


		// sweep options

		if (mb::same_string(*it, "--sweep_option")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "EN_Td")) {
				sweep_p.sweep_option = mb::sweep_option::EN_Td;
			}
			else if (mb::same_string(arg, "T_K")) {
				sweep_p.sweep_option = mb::sweep_option::T_K;
			}
			else if (mb::same_string(arg, "p_Torr")) {
				sweep_p.sweep_option = mb::sweep_option::p_Torr;
			}
			else if (mb::same_string(arg, "bin_frac")) {
				sweep_p.sweep_option = mb::sweep_option::bin_frac;
			}
			else if (mb::same_string(arg, "Nu")) {
				sweep_p.sweep_option = mb::sweep_option::Nu;
			}
			else if (mb::same_string(arg, "N_terms")) {
				sweep_p.sweep_option = mb::sweep_option::N_terms;
			}
			else {
				mb_cmd::bad_arg(arg);
			}

			it = it + 1;
			continue;
		}

		
		if (mb::same_string(*it, "--sweep_style")) {
			arg = argv.at(this_idx + 1); // next 1 arg

			if (mb::same_string(arg, "def")) {
				sweep_p.sweep_style = mb::sweep_style::def;


				// find all subseqent arguments which are either integers or floats.
				for (auto it2 = it + 2; it2 != argv.end(); it2++) {

					std::string argx = *it2;

					if (mb::isFloat(argx)) {
						sweep_p.defval.push_back(mb_cmd::enforce_is_float(argx));
					}
					else {
						break;
					}
				}

				it = it + sweep_p.defval.size() + 1;
				continue;

			}
			else {
				if (mb::same_string(arg, "lin")) {
					sweep_p.sweep_style = mb::sweep_style::lin;
				}
				else if (mb::same_string(arg, "log")) {
					sweep_p.sweep_style = mb::sweep_style::log;
				}
				else if (mb::same_string(arg, "reg")) {
					sweep_p.sweep_style = mb::sweep_style::reg;
				}
				else {
					mb_cmd::bad_arg(arg);
				}

				std::string arg2 = argv.at(this_idx + 2);
				std::string arg3 = argv.at(this_idx + 3);
				std::string arg4 = argv.at(this_idx + 4);

				sweep_p.start = mb_cmd::enforce_is_float(arg2);
				sweep_p.stop = mb_cmd::enforce_is_float(arg3);
				sweep_p.points = mb_cmd::enforce_is_int(arg4); // todo: this is unintuitive when it comes to reg, be careful

				it = it + 4;
				continue;
				
			}

		}


		// Export parameters

		if (mb::same_string(*it, "--NO_EXPORT")) {
			exp_p.NO_EXPORT = true;

			continue;
		}

		if (mb::same_string(*it, "--LIMIT_EXPORT")) {
			exp_p.LIMIT_EXPORT = true;

			continue;
		}

		if (mb::same_string(*it, "--export_location")) {
			arg = argv.at(this_idx + 1); // next 1 arg

			exp_p.export_location = arg;

			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--export_name")) {
			arg = argv.at(this_idx + 1); // next 1 arg

			if (! mb::same_string(mb::trim_copy(arg), "<default>")) {
				exp_p.export_name = arg;
			}
			

			it = it + 1;
			continue;
		
		}

		if (mb::same_string(*it, "--EXPORT_XSECS")) {
			exp_p.EXPORT_XSECS = true;

			continue;
		}

		


		// detect SILENT op
		if (mb::same_string(*it, "--SILENT")) {
			SILENT = true;
			continue;
		}

		if (mb::same_string(*it, "--multibolt_num_threads")) {
			arg = argv.at(this_idx + 1); // next 1 arg

			#ifdef MULTIBOLT_USING_OPENMP

				if (mb::same_string(arg, "max")) {
					multibolt_num_threads = omp_get_max_threads();
				}
				else {
					multibolt_num_threads = mb_cmd::enforce_is_int(arg);
				}
			#else

			mb::normal_statement("MultiBolt was compiled without OpenMP; Max thread count will remain 1.");
				multibolt_num_threads = 1;
			#endif
			

			it = it + 1;
			continue;

		}
		


		// detect scattering style ops
		if (mb::same_string(*it, "--elastic_scattering")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "Isotropic")) {
				lib_p.elastic_scattering = lib::isotropic_scattering();
			}
			else if (mb::same_string(arg, "ScreenedCoulomb")) {
				std::string arg2 = argv.at(this_idx + 2); // next 1 arg
				double SCREEN_EV = mb::HARTREE;
				if (mb::isFloat(arg2)) {
					SCREEN_EV = mb_cmd::enforce_is_float(arg2);
					it = it + 1;
				}
				lib_p.elastic_scattering = lib::screened_coulomb_scattering(SCREEN_EV);
			}
			else {
				mb_cmd::bad_arg(arg);
			}
			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--excitation_scattering")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "Isotropic")) {
				lib_p.excitation_scattering = lib::isotropic_scattering();
			}
			else if (mb::same_string(arg, "IdealForward")) {
				lib_p.excitation_scattering = lib::forward_scattering();
			}
			else if (mb::same_string(arg, "ScreenedCoulomb")) {
				std::string arg2 = argv.at(this_idx + 2); // next 1 arg
				double SCREEN_EV = mb::HARTREE;
				if (mb::isFloat(arg2)) {
					SCREEN_EV = mb_cmd::enforce_is_float(arg2);
					it = it + 1;
				}
				lib_p.excitation_scattering = lib::screened_coulomb_scattering(SCREEN_EV);
			}
			else {
				mb_cmd::bad_arg(arg);
			}
			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--ionization_scattering")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "Isotropic")) {
				lib_p.ionization_scattering = lib::isotropic_scattering();
			}
			else if (mb::same_string(arg, "IdealForward")) {
				lib_p.ionization_scattering = lib::forward_scattering();
			}
			else if (mb::same_string(arg, "ScreenedCoulomb")) {
				
				std::string arg2 = argv.at(this_idx + 2); // next 1 arg
				double SCREEN_EV = mb::HARTREE;
				if (mb::isFloat(arg2)) {
					 SCREEN_EV = mb_cmd::enforce_is_float(arg2);
					 it = it + 1;
				}
				lib_p.ionization_scattering = lib::screened_coulomb_scattering(SCREEN_EV);

			}
			else {
				mb_cmd::bad_arg(arg);
			}
			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--superelastic_scattering")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "Isotropic")) {
				lib_p.superelastic_scattering = lib::isotropic_scattering();
			}
			else if (mb::same_string(arg, "IdealForward")) {
				lib_p.superelastic_scattering = lib::forward_scattering();
			}
			else if (mb::same_string(arg, "ScreenedCoulomb")) {
				std::string arg2 = argv.at(this_idx + 2); // next 1 arg
				double SCREEN_EV = mb::HARTREE;
				if (mb::isFloat(arg2)) {
					SCREEN_EV = mb_cmd::enforce_is_float(arg2);
					it = it + 1;
				}
				lib_p.superelastic_scattering = lib::screened_coulomb_scattering(SCREEN_EV);
			}
			else {
				mb_cmd::bad_arg(arg);
			}
			it = it + 1;
			continue;
		}

		if (mb::same_string(*it, "--interp_method")) {
			arg = argv.at(this_idx + 1); // next 1 arg
			if (mb::same_string(arg, "Linear")) {
				p.interp_method = lib::InterpolationMethod::Linear;
			}
			else if (mb::same_string(arg, "Logarithmic")) {
				p.interp_method = lib::InterpolationMethod::Logarithmic;
			}
			else {
				mb_cmd::bad_arg(arg);
			}
			it = it + 1;
			continue;
		}



		// unrecognized argument case
		mb_cmd::bad_arg(*it);


	}


	if (SILENT) {
		mb::mbSpeaker.printmode_no_statements();
	}


			
	return std::tuple<mb::BoltzmannParameters, mb_cmd::LibraryParams, mb_cmd::SweepParams, mb_cmd::ExportParams, int> {p, lib_p, sweep_p, exp_p, multibolt_num_threads};


}



