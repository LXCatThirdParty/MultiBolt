// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// 03/10/2023
// Use Argon (known elastic TCS and MTCS) and Okhrimovskyy scattering to implement 
// the variation on Screened Coulomb scattering that better resembles the real DCS
//
// Refer to: 10.1103/PhysRevE.65.037402
// A. Okhrimovskyy et al, "Electron anisotropic scattering in gases: A formula for Monte Carlo simulations,�
// Physical Review E, vol. 65, no. 3, p. 37402, 2002.

#include "multibolt"

// For the sake of keeping data available, never poofing
struct XI_struct {
	arma::vec eV;
	arma::vec XI;
	XI_struct() {
		arma::mat data;// = arma::mat(456, 2, arma::fill::zeros);
		data.load("../../applications/Ar_scattering/Ar_xi.txt");

		this->eV = data.col(0);
		this->XI = data.col(1);
	}
};
const XI_struct this_XI_struct = XI_struct();


// Interpolate XI data
double foo_XI(const double eV) {
	// assume XI is constant outside the given data
	if (eV <= this_XI_struct.eV(1)) {
		return this_XI_struct.XI(1);
	}
	else if (eV >= this_XI_struct.eV(this_XI_struct.eV.n_elem - 1)) {
		return  this_XI_struct.XI(this_XI_struct.XI.n_elem - 1);
	}

	arma::vec v_XI;
	arma::vec v_eV = { eV };
	arma::interp1(this_XI_struct.eV, this_XI_struct.XI, v_eV, v_XI);

	return v_XI.at(0);
}



// Same as screened coulomb scattering, but Xi is a function derived from the ratio between total and momentum-transfer
// elastic collision cross sections
lib::Scattering Ar_Okhrimovskyy_scattering() {

	std::function<double(const double, const double, const double)> foo_Ibar =
		[](const double eV, const double eVprime, const double chi) {
	
		double this_XI = foo_XI(eV);
		return 1.0 / (4 * lib::PI) * (1 - this_XI * this_XI) / pow((1 - this_XI * cos(chi)), 2);
	  };
	
	std::function<double(const double, const double)> foo_ratio = // ratio of cross sections just needs incident electron energy
		[](const double eV, const double eVprime) {
	
		double this_XI = foo_XI(eV);

		if (this_XI == 0) {
			return 1.0;
		}

		double ratio = (1 - this_XI) / (2 * this_XI * this_XI) * ((1 + this_XI) * log((1 + this_XI) / (1 - this_XI)) - 2 * this_XI);
		
		if (arma::arma_isnan(ratio)) {
			std::cout << this_XI << std::endl;
		}


		return (1.0 / ratio);
	};
	
	lib::Scattering X = lib::Scattering(foo_Ibar);
	X.name = "Okhrimovskyy(Ar)";
	
	X.user_ratio_elasticTotal_to_elasticMT = foo_ratio;
	X.ratio_USE_SLOW_EVAL = false;

	X.cos_chi = [](const double R, const double eV, const double eVprime) {
	
		double this_XI = foo_XI(eV);
		double cos_chi = 1 - (2*R*(1 - this_XI)) / (1 + this_XI * (1 - 2*R));
		return cos_chi;
	};

	X.find_better_grid_chi(this_XI_struct.eV, this_XI_struct.eV);
	
	return X;
}






int main() {



	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

	// Form library and correct original file

	lib::Library Ar_lib = lib::Library();
	Ar_lib.add_xsecs_from_LXCat_files("../../applications/Ar_scattering/BSR_Ar_20230222.txt");

	Ar_lib.assign_fracs_by_names("Ar", 1.0);
	Ar_lib.remove_all_zero_frac();

	int ix_is_total_elastic = 0;
	int ix_is_mt_elastic = 1;

	Ar_lib.allspecies[0]->ela[ix_is_mt_elastic]->process("E + Ar -> E + Ar, Elastic"); // corrected form for recognition
	Ar_lib.allspecies[0]->erase_by_index(ix_is_total_elastic);

	ix_is_mt_elastic = 0; ix_is_total_elastic = -1; // for safety, reference

	// ---------------------------

	lib::Scattering Ar_elastic_scattering = Ar_Okhrimovskyy_scattering();
	lib::Scattering Ar_inelastic_scattering = lib::screened_coulomb_scattering();




	

	//arma::vec ell = { 0, 1, 2, 3, 4, 5 };
	//arma::vec eV = arma::logspace(log10(0.0014), log10(300), 100);
	//
	//arma::mat printme = arma::mat(eV.n_elem, ell.n_elem, arma::fill::zeros);
	//
	//for (int i = 0; i < ell.n_elem; ++i) {
	//	printme.col(i) = Ar_elastic_scattering.scattering_integral(eV, eV, ell(i));
	//}
	//
	//printme.print();

	//	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE;
	p.N_terms = 2; // low accuracy for testing


	p.conv_err = 1e-5;

	p.Nu = 200; // very low res for testing, only expect good results for larger Nu
	p.weight_f0 = 1.0;	// Iteration weight. 

	p.iter_max = 200;

	p.initial_eV_max = 50;
	p.USE_EV_MAX_GUESS = true;
	p.USE_ENERGY_REMAP = true;		// Look for a new grid if necessary
	p.remap_grid_trial_max = 5;				// try a new grid no more than this many times
	p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)
	p.iter_min = 1;

	p.p_Torr = 760;
	p.T_K = 300;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization


	std::vector<lib::Scattering> vec_ela_scattering =
	{ lib::isotropic_scattering(), Ar_elastic_scattering, lib::screened_coulomb_scattering() };
	std::vector<lib::Scattering> vec_inel_scattering =
	{ lib::isotropic_scattering(), Ar_inelastic_scattering, lib::forward_scattering() };

	

	/* Perform Boltzmann solver calculation  */


	mb::normal_statement("Using following parameters as starting-point:");
	p.print();




	for (auto& x : Ar_lib.allspecies[0]->allcollisions) {

		if (x->code() == lib::CollisionCode::elastic) {
			x->scattering(vec_ela_scattering[1]);
		}
		else {
			x->scattering(vec_inel_scattering[1]);
		}
	}

	
	Ar_lib.print();

	arma::vec EN_Td = arma::logspace(-3, 3, 10);
	std::vector<mb::BoltzmannOutput> out_vec(EN_Td.n_elem);

	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < EN_Td.n_elem; ++i) {
	
		mb::BoltzmannParameters this_p = p;
		this_p.EN_Td = EN_Td.at(i);

		lib::Library this_Ar_lib = Ar_lib;

		out_vec.at(i) = mb::BoltzmannSolver(this_p, this_Ar_lib).get_output();
	}


	mb::Exporter exp = mb::Exporter();

	auto sweep_option = mb::sweep_option::EN_Td;
	bool export_xsecs = false;
	bool LIMIT_EXPORT = false;

	std::string export_location = "../../applications/Ar_scattering/export";
	std::string export_name = mb::UNALLOCATED_STRING;

	exp = mb::Exporter(Ar_lib, p, out_vec.at(0),
		export_location, export_name, sweep_option, export_xsecs, LIMIT_EXPORT);

	for (int i = 0; i < EN_Td.n_elem; ++i) {
		p.EN_Td = EN_Td.at(i);
		exp.write_this_run(p, out_vec.at(i));
	}

	std::cout << std::endl;


	return 0;

	
}

