% Addition to MultiBolt
% Max Flynn, 2018
% Remap the grid in a fashion which more closely resembles that of BOLSIG+.
% Instead of focusing on the raw height of the tail, look at how many orders are spanned instead.
% To use this script, move this script where necessary and edit the MATLAB scripts to call *this* one instead of Remap_Energy.m

%--------- Remap the energy grid --------%

% open to user choice
TargetOrderSpan = 10; % New Remap: Controls desired span of EEDF while remapping

u_max_prev = u_max;


[fbody, f_idx] = unique(f_0);
fsize = size(fbody, 2);

% try to avoid possible grid blips
head_idx = floor(0.01*fsize);
tail_idx = floor(0.95*fsize); 

f_idx = f_idx(f_idx >= head_idx & f_idx <= tail_idx);

fbody = f_0(f_idx); % fbody is now the unique points, and just-inside
% and f_idx is tracking those points


% how many orders are currently spanned across
OrderSpan = log10(max(fbody)) - log10(min(fbody));


if OrderSpan > TargetOrderSpan + 1 % if too many orders are covered
    % try decreasing the gridmax
  
    TargetOrder = log10(max(fbody)) - TargetOrderSpan;
    TargetTail = 10^(TargetOrder);
    
    u_max = interp1(fbody, ue(f_idx), 10^floor(log10(TargetTail)), 'linear', 'extrap');
   
    Du = u_max/(Nu-1);      
    uo(uv) = (uv)*Du;       
    ue(uv) = (uv-1/2)*Du;
    
elseif OrderSpan < TargetOrderSpan / 2    % if very few orders are covered, needs drastic change
    
    u_max = u_max * 2;
    Du = u_max/(Nu-1);      
    uo(uv) = (uv)*Du;       
    ue(uv) = (uv-1/2)*Du;
    
elseif OrderSpan < TargetOrderSpan - 1 % if too few orders are covered, but not wildly so
    % try increasing the gridmax
    
    u_max = u_max * 1.1;
    Du = u_max/(Nu-1);      
    uo(uv) = (uv)*Du;       
    ue(uv) = (uv-1/2)*Du;
    
end



if u_max ~= u_max_prev
    REMAPPED = 1;
    for Ng = 1:N_gases      % Interpolate cross-sections to the new energy grids 
        fid = fopen(string(Xsec_fids(Ng)));     
        Read_Xsecs          % Run the cross-sections import script (Reads the LXCat file, and interpolates the Xsecs to the numerical grid)
    end
    s_Te_eff(uv) = 0;
    for Ng = 1:N_gases
        s_Te_eff(uv) = s_Te_eff(uv) + squeeze(s_Te_gas(uv,Ng))';
    end
end