% Jacob Stephens, MIT 2018
% -------------------------- MultiBolt_v2.01 ---------------------------- %

omega1 = 1/3*sqrt(2/me/qe)*trapz(uo(uvt)/qe,uo(uvt).*f_1(uvt)); % Initial guess, not actually omega_1
Gamma_1L = w;

for iter2 = 1:iter_max
    if iter2 == 1
    A_f1L(1:Nu*N_terms,1:Nu*N_terms) = 0;
        for l1 = 0:N_terms-1
            for k1 = 1:Nu       % row #
                for k2 = 1:Nu   % column #
                    if l1 == 0  % Use the l='even' eqs.
                        if k1 == k2 
                            A_f1L(k1,k2) = +HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5); % f_0(k)
                            A_f1L(k1,k2+Nu) = qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                             +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                             +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;     % f_1(k)  
                        elseif k1 == k2 + 1
                            A_f1L(k1,k2+Nu) = -qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                              +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;  % f_1(k-1) 
                        end
                        for Ng = 1:N_gases
                            if k1 == k2 
                                A_f1L(k1,k2) = A_f1L(k1,k2) + frac(Ng)*2*me/mN(Ng)/Du*ue(k1)^2*Np*s_me(k1,Ng) ...
                                                            + frac(Ng)*ue(k1)*Np*(s_Te(k1,Ng)-s_me(k1,Ng));
                            elseif k1 == k2 - 1
                                A_f1L(k1,k2) = A_f1L(k1,k2) - frac(Ng)*2*me/mN(Ng)/Du*ue(k2)^2*Np*s_me(k2,Ng);     % f_0(k+1)
                            end
                            for i_exc = 1:N_excs(Ng)
                                if (k2 == k1 + dk_exc(i_exc,Ng))
                                    A_f1L(k1,k2) = A_f1L(k1,k2) - frac(Ng)*ue(k2)*Np*s_exce(k2,i_exc,Ng);       % f_0(k+dk_exc)  
                                end
                            end
                            for i_iz = 1:N_izs(Ng)
                                if (k2==2*k1+dk_ize(i_iz,Ng) || k2==2*k1+dk_ize(i_iz,Ng)-1)
                                    A_f1L(k1,k2) = A_f1L(k1,k2) - frac(Ng)*2*ue(k2)*Np*s_ize(k2,i_iz,Ng);                  % f_0(k+dk_iz_eff)
                                end
                            end
                        end
                    elseif 1 <= l1 && l1 < N_terms-1
                        if mod(l1,2) == 0 % l1='even'
                            if k1 == k2
                                for Ng = 1:N_gases
                                    A_f1L(k1+l1*Nu,k2+l1*Nu) = A_f1L(k1+l1*Nu,k2+l1*Nu) ...
                                                              +frac(Ng)*ue(k1)*Np*s_Te(k1,Ng);   % f_l(k)
                                end
                                A_f1L(k1+l1*Nu,k2+l1*Nu) =  A_f1L(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                                A_f1L(k1+l1*Nu,k2+Nu+l1*Nu) = +qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                              +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;     % f_(l+1)(k)  
                                A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) = +qe*E0*l1/(2*l1-1)*ue(k1)/Du ...
                                                              -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                              +l1/(2*l1-1)*ue(k1)/2*alpha_N*Np*SST;  % f_(l-1)(k)
                            elseif k1 == k2 + 1
                                A_f1L(k1+l1*Nu,k2+Nu+l1*Nu) = -qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                              +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST; % f_(l+1)(k-1)
                                A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) = -qe*E0*l1/(2*l1-1)*ue(k1)/Du ...
                                                              -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                              +l1/(2*l1-1)*ue(k1)/2*alpha_N*Np*SST;    % f_(l-1)(k-1)                                
                            end
                        else % l1 ='odd'
                            if k1 == k2 
                                for Ng = 1:N_gases
                                    A_f1L(k1+l1*Nu,k2+l1*Nu) = A_f1L(k1+l1*Nu,k2+l1*Nu) ...
                                                             + frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                                end
                                A_f1L(k1+l1*Nu,k2+l1*Nu) =  A_f1L(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)                 
                                A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) =  -qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                               -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                               +l1/(2*l1-1)*uo(k1)/2*alpha_N*Np*SST;      % f_(l-1)(k)
                                A_f1L(k1+l1*Nu,k2+Nu+l1*Nu) =  -qe*E0*(l1+1)/(2*l1+3)*uo(k1)/Du ...
                                                               +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                               +(l1+1)/(2*l1+3)*uo(k1)/2*alpha_N*Np*SST;  % f_(l+1)(k)
                            elseif k1 == k2 - 1
                                A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) = +qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                              -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                              +l1/(2*l1-1)*uo(k1)/2*alpha_N*Np*SST;       % f_(l-1)(k+1)
                                A_f1L(k1+l1*Nu,k2+Nu+l1*Nu) = +qe*E0*(l1+1)/(2*l1+3)*uo(k1)/Du ...
                                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                              +(l1+1)/(2*l1+3)*uo(k1)/2*alpha_N*Np*SST;   % f_(l+1)(k+1)
                            end
                        end
                    elseif  l1 == N_terms-1 % l1='odd'
                        if k1 == k2 
                            for Ng = 1:N_gases
                                A_f1L(k1+l1*Nu,k2+l1*Nu) = A_f1L(k1+l1*Nu,k2+l1*Nu) ...
                                                         + frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                            end
                            A_f1L(k1+l1*Nu,k2+l1*Nu) =  A_f1L(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)     
                            A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) = -qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                          -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;  % f_(l-1)(k)
                        elseif k1 == k2 - 1
                            A_f1L(k1+l1*Nu,k2+l1*Nu) = 0;       % f_l(k+1)
                            A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) = qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                         -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;     % f_(l-1)(k+1)
                        elseif k1 == k2 + 1
                            A_f1L(k1+l1*Nu,k2+l1*Nu) = 0;     % f_l(k-1)
                            A_f1L(k1+l1*Nu,k2-Nu+l1*Nu) = 0;  % f_(l-1)(k-1)
                        end
                    end
                end
            end
        end
    end
    b_f1L(1:Nu*N_terms) = 0;
    for l1 = 0:N_terms-1
       for k1 = 1:Nu 
           if l1 == 0
               if k1 ~= 1
                   b_f1L(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega1*x_f(k1) + (l1+1)/(2*l1+3)*ue(k1)*(x_f(k1+Nu)+x_f(k1+Nu-1))/2;
               else
                   b_f1L(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega1*x_f(k1) + (l1+1)/(2*l1+3)*ue(k1)*x_f(k1+Nu)/2;
               end
           elseif 1 <= l1 && l1 <= N_terms-2
               if mod(l1,2) == 0 % l1='even'
                   if k1 ~= 1
                       b_f1L(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega1*x_f(k1+l1*Nu) + l1/(2*l1-1)*ue(k1)*(x_f(k1+l1*Nu-Nu)+x_f(k1+l1*Nu-Nu-1))/2 + (l1+1)/(2*l1+3)*ue(k1)*(x_f(k1+l1*Nu+Nu)+x_f(k1+l1*Nu+Nu-1))/2;
                   else
                       b_f1L(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega1*x_f(k1+l1*Nu) + l1/(2*l1-1)*ue(k1)*x_f(k1+l1*Nu-Nu)/2 + (l1+1)/(2*l1+3)*ue(k1)*x_f(k1+l1*Nu+Nu)/2;
                   end
               else
                   if k1 ~= Nu
                       b_f1L(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega1*x_f(k1+l1*Nu) + l1/(2*l1-1)*uo(k1)*(x_f(k1+l1*Nu-Nu)+x_f(k1+l1*Nu-Nu+1))/2 + (l1+1)/(2*l1+3)*uo(k1)*(x_f(k1+l1*Nu+Nu)+x_f(k1+l1*Nu+Nu+1))/2;
                   else
                       b_f1L(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega1*x_f(k1+l1*Nu) + l1/(2*l1-1)*uo(k1)*x_f(k1+l1*Nu-Nu)/2 + (l1+1)/(2*l1+3)*uo(k1)*x_f(k1+l1*Nu+Nu)/2;
                   end
               end
           elseif l1 == N_terms-1
               if k1 ~= Nu
                   b_f1L(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega1*x_f(k1+l1*Nu) + l1/(2*l1-1)*uo(k1)*(x_f(k1+l1*Nu-Nu)+x_f(k1+l1*Nu-Nu+1))/2;
               else
                   b_f1L(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega1*x_f(k1+l1*Nu) + l1/(2*l1-1)*uo(k1)*x_f(k1+l1*Nu-Nu)/2;
               end
           end
       end
    end
    b_f1L = b_f1L*Np;

    % Normalization condition
    A_f1L(Nu,1:Nu*N_terms) = 0;   
    A_f1L(Nu,uv) = (((ue/qe).^0.5)*Du/qe);
    b_f1L(Nu) = 0;

    % b_f1z = b_f1z;
    A_f1L = sparse(A_f1L);
    x_f1L = (A_f1L\(b_f1L')); % Solve the system
    f1L_1(1:Nu) = x_f1L((Nu+1):2*Nu);
    DFL_N = 1/3*sqrt(2/me/qe)*trapz(uo/qe,uo.*f1L_1); % Flux portion of DL_N

    f1L_0 = x_f1L(1:Nu)';
    S_1L = 0;
    S_1L_att = 0;
    for Ng = 1:N_gases
        for i_att = 1:N_atts(Ng)
            S_1L_att(i_att,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,squeeze(frac(Ng)*s_atte(uv,i_att,Ng))'.*f1L_0(uv).*(ue(uv).^1.0/qe));
        end
        for i_iz = 1:N_izs(Ng)
            S_1L_iz(i_iz,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,squeeze(frac(Ng)*s_ize(uv,i_iz,Ng))'.*f1L_0(uv).*(ue(uv).^1.0/qe));
        end
    end
    S_1L = sum(sum(S_1L_iz)) - sum(sum(S_1L_att));    
    omega1_NEW = Gamma_1L + S_1L;
    if abs(abs(abs(omega1-omega1_NEW)/omega1_NEW))<conv_err && iter2 > 5 % iterate until omega1 converges
        omega1 = omega1_NEW;
        converged_f1L(is) = 1; % Useful flag for user feedback
        break
    elseif abs(abs(abs(omega1-omega1_NEW)/omega1_NEW))>conv_err && iter2 == iter_max
        disp('Warning: f_1L solution did not converge')
        converged_f1L(is) = 0;
    end
    omega1 = omega1_NEW;
end
