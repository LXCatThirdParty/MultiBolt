% Jacob Stephens, MIT 2018
% Email: jastephe15@gmail.com
% 
% If you use the code, please cite,
% J. Stephens, J. Phys. D: Appl. Phys. 51, 125203 (2018).
% DOI: https://doi.org/10.1088/1361-6463/aaaf8b
% 
% Also properly cite the LXcat database and utilized cross-sections!!!
% 
% -------------------------- MultiBolt_v2.01 ---------------------------- %
% This MATLAB code, and accompanying scripts make up the framework of the
% multi-term Boltzman equation (BE) solver, MultiBolt. In this version, the
% BE is decomposed into an arbitrary number of spherical harmonics and
% second order density gradients, as discussed by Robson and Ness, PRA
% 1986. The number of spherical harmonics terms, N_terms, may be modified 
% to achieve the desired accuracy. By definition, N_terms>2 is a multi-term 
% BE model.
%
% This work was completed mostly as a hobby, resulting in the code
% recieving limited development. There is certainly much room for 
% improvement. I am distributing the code in good faith to the community, 
% with the hopes that some people may find it useful in their work. Modify 
% the code as you see fit. I make no promises to the accuracy of the code 
% and assume no liability for its use. 
% 
% Special thanks to Mohamed Rabie and Christian Franck (ETH Zurich) for
% allowing their METHES LXcat cross-section import script to be shared with
% this code! 
% M. Rabie, C.M. Franck, Comp. Phys. Comm. 203, 268-277, (2016).
%
% Cheers and best of luck!
% Jacob 
%
% --------------------- Brief Development History ----------------------- %
% -------------------------- MultiBolt_v1.01 ---------------------------- %
% 
% i. Original code. Basic multi-term Legendre polynomial expansion of the
% uniform BE.
% ii. "Pulsed Townsend" or "Steady-State Townsend" growth models only.
% 
% -------------------------- MultiBolt_v1.02 ---------------------------- %
% 
% i. First order density gradient expansion added. 
% 
% -------------------------- MultiBolt_v1.03 ---------------------------- %
% This is the version of the code that was used for initial J. Phys. D
% publication.
% 
% i. Second order density gradient expansion added, along with calculation 
% of bulk transport coefficients (except D_T).
%
% ii. Code verified using the Reid Ramp model, Lucas-Salee model, and the
% attachment model of Robson and Ness.
% 
% -------------------------- MultiBolt_v2.01 ---------------------------- %
% First version of the publicly distributed code.
% 
% i. Complete spherical harmonics expansion to replace to the previous
% Legendre expansion (Robson, Ness PRA 1986). Can calculate all 2nd order
% bulk transport coefficients.
% 
% ii. Code modified to model transport in gas mixtures for an arbitrary
% number of gases.
% 
% iii. Code verified using the Reid Ramp model, Lucas-Salee model, and the
% attachment model of Robson and Ness.
% 
% iv. Calculated transport and rate coefficients are output into tables
% versus E/N automatically.
%
% v. Code will attempt to find and utilize the "best" upper limit for the
% considered electron energy dynamically. The upper limit is taken to be
% where the EEDF falls to ~1E-12.
% 
% ----------------------------------------------------------------------- %
% ----------------------------------------------------------------------- %

% Most units are SI (I have tried to comment everywhere that this is not the case)
clear
tic
%--------- Constants --------%
kB = 1.38E-23;    % Boltzmann constant 
qe = 1.602E-19;   % Elementary charge
me = 9.11E-31;    % Mass of the electron
AMU = 1.66E-27;   % Atomic mass unit         
%----------------------------%
is = 0;                           
EN_Td_run = linspace(100,500,5);     % Electric field [Td] % Can add a sweep here, e.g. EN_Td = [50 100 150 200], EN_Td = logspace(1,3,10), or linspace(1,500,100), etc.
p_Torr = 200;                        % Pressure [Torr]. Not important since the solutions are a function of E/N only.
N_terms = 4;                         % Number of spherical harmonics used in the expansion (must be even! [2, 4, 6, 8, 10, etc.])
Nu = 2000;                           % Number of energy points in the finite difference scheme
% ---- Simulation Flags ---- % (See documentation)
ENERGY_REMAP = 1;  % Always use integers [0,1]. For ENERGY_REMAP=1, MultiBolt will determine the maximum energy for the numerical grid.
SST = 0;           % Always use integers [0,1]. For SST=1, MultiBolt will calculate the distribution function for an exponential trend in density.
HD = 1;            % Always use integers [0,1]. For HD=1, MultiBolt will calculate the distribution function corresponding to a density 
GE = 1;            % Always use integers [0,1]. GE=1 enables gradient expansion to find bulk drift velocity, D_L, and D_N. If you do not care about these variables, set GE=0.
fNEW = 0.3;        % Variable for updating iterative solver. Use (0,1), and modify for best convergence.
conv_err = 5E-4;   % Simulation will iterate until the relative change in mean energy or omega is less than this number
iter_max = 100;    % The simulation will iterate until it's converged, or until it hits iter_max. Modify accordingly.

% ---------------- Cross-section info (LXCat) ------------------- %
% A few notes for the cross-sections files
% 1. Include only the elastic momentum transfer X-sec. Do not include the 'total', 'effective', etc.
% 2. Include only (neutral)+e --> ? reactions. No three-body cross-sections, or cross-sections for interaction with non-neutrals.

N_gases = 2;                     % Total number of gases in the mixture
frac = [0.78 0.22];              % Fractional composition of the gas mixture
mN = [28 32]*AMU;                % Mass of the neutrals 
Xsec_fids = {'../Cross-Sections/Biagi_N2.txt', ...
             '../Cross-Sections/Biagi_O2.txt'}; % File locations for each cross-section set
Xsec_disp_flag = 0;  

% --- variable initialization --- % 
k_iz = 0;
k_att = 0;
avg_en = 0;
for EN_Td = EN_Td_run          
    is = is + 1;            % iterate output index
    clear A_* b_* f_*
    
    %---------------------------------------------------------------------%
    % If ENERGY_REMAP=0, user should carefully select u_max to suit their needs!
    u_max = EN_Td*qe;    % Maximum considered electron energy [J]. The choice of EN_Td*qe, is purely empirical, based on my experience. Users may find better approximations.
    %---------------------------------------------------------------------%
    
    Np = 101325/kB/300*(p_Torr/760);  % Background density of neutrals calculated for 300 K
    E0 = Np*EN_Td*1E-21;              % Electric field magnitude [V/m] for the specified E/N and pressure               
    uv = 1:Nu;                        % Vector of indices for the energy grid
    uvt = 1:Nu-1;                     % Truncated vector of indices for the energy grid (useful for finite difference scheme)
    Du = u_max/(Nu-1);                % Magnitude of the energy step in the finite difference scheme   
    % Note that the l='even' and l='odd' energy grids are a half step staggered in energy from each other
    uo(uv) = (uv)*Du;                 % Vector of energy points for the l='odd' grid 
    ue(uv) = (uv-1/2)*Du;             % Vector of energy points for the l='even' grid

    for Ng = 1:N_gases
        fid = fopen(string(Xsec_fids(Ng)));     
        Read_Xsecs % Run the cross-sections import script (Reads the LXCat file, and interpolates the Xsecs to the numerical grid)
    end
    s_Te_eff(uv) = 0;
    for Ng = 1:N_gases
        s_Te_eff(uv) = s_Te_eff(uv) + squeeze(s_Te_gas(uv,Ng))';
    end
    
    % --- Variables requiring iteration --- %
    k_iz_eff = 0;
    alpha_N = 0;
    
    if SST == 1
        GE = 0; % Hard over-ride. Gradient expansion for a SST simulation is meaningless. Do not calculate gradient expansion terms from f^SST!
        HD = 0; % Never run a simulation with HD=1 and SST=1 (no physical meaning).
    end
    if N_terms == 2
        GE = 0; % Cannot perform gradient expansion if only two terms are used in the spherical harmonics expansion!
    end
    if HD==0 && SST==0 
        iter_max = 1; 
    end
    
    flag1_file = 0;
    
    for iter1 = 1:iter_max
        
        if iter1 == 1 || REMAPPED == 1
            REMAPPED = 0;
            Solve_f % For HD==1, this will solve for f^0. For SST==1, this will solve for f^SST
        else
            UpdateSolve_f;
        end
            
        Calculate_Rates % Calculates basic rate and transport coefficients
        if ENERGY_REMAP == 1
            Remap_Energy
        end  
        
        % Plot_EEDF % Can uncomment this to plot the EEDF

        if abs(1-avg_en_prev/avg_en)<conv_err && iter1 > 6 % Check for convergence
            converged_f(is) = 1; % Useful flag for user feedback
            if GE == 1 % If true, calculate f1T, f1L, f2T, and f2L to calculate transport coefficients
                Solve_f1T
                Solve_f1L
                Solve_f2T
                Solve_f2L
                w_BULK = omega1;
                DT_BULK = (1/sqrt(3))*(omega2+1/sqrt(2)*omega2_bar);
                DL_BULK = (1/sqrt(3))*(omega2-sqrt(2)*omega2_bar); 
            end
            Export_Data
            break
        else
            converged_f(is) = 0;
            converged_f1L(is) = 0; 
            converged_f2T(is) = 0; 
            converged_f2L(is) = 0;
        end

        if iter1 == iter_max && GE == 1 % If true, calculate f1T, f1L, f2T, and f2L to calculate transport coefficients
            converged_f(is) = 0;
            disp('Warning: Solution did not converge')
            Solve_f1T
            Solve_f1L
            Solve_f2T
            Solve_f2L
            w_BULK = omega1;
            DT_BULK = (1/sqrt(3))*(omega2+1/sqrt(2)*omega2_bar);
            DL_BULK = (1/sqrt(3))*(omega2-sqrt(2)*omega2_bar); 
            Export_Data
        elseif iter1 == iter_max && GE ~= 1
            converged_f(is) = 0;
            converged_f1L(is) = 0; 
            converged_f2T(is) = 0; 
            converged_f2L(is) = 0;
            disp('Warning: Solution did not converge')
            Export_Data
        end
    end
end
disp(['Data Exported to Run ID ',num2str(RUN_ID)])
toc