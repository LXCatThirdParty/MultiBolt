% Jacob Stephens, MIT 2018
% -------------------------- MultiBolt_v2.01 ---------------------------- %

A_f1T(1:Nu*(N_terms-1),1:Nu*(N_terms-1)) = 0;
for l1 = 1:N_terms-1
    for k1 = 1:Nu       % row #
        for k2 = 1:Nu   % column #
            if l1 == 1
                if k1 == k2 
                    for Ng = 1:N_gases
                        A_f1T(k1,k2) = A_f1T(k1,k2)+frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                    end
                    A_f1T(k1,k2) = A_f1T(k1,k2) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                    A_f1T(k1,k2+Nu) =  -qe*E0*(l1+2)/(2*l1+3)*uo(k1)/Du ...
                                       +qe*E0*(l1+2)/(2*l1+3)*(l1+2)/2/2;  % f_(l+1)(k)
                elseif k1 == k2 - 1
                    A_f1T(k1,k2) = 0;     % f_0(k+1)  
                    A_f1T(k1,k2+Nu) = +qe*E0*(l1+2)/(2*l1+3)*uo(k1)/Du ...
                                      +qe*E0*(l1+2)/(2*l1+3)*(l1+2)/2/2;   % f_(l+1)(k+1)
                end
            elseif 2 <= l1 && l1 < N_terms-1
                if mod(l1,2) == 0 % l1='even'
                    if k1 == k2
                        for Ng = 1:N_gases
                            A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) = A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) ...
                                                              +frac(Ng)*ue(k1)*Np*s_Te(k1,Ng);   % f_l(k)
                        end
                        A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) =  A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) + HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                        A_f1T(k1+(l1-1)*Nu,k2+Nu+(l1-1)*Nu) = +qe*E0*(l1+2)/(2*l1+3)*ue(k1)/Du ...
                                                              +qe*E0*(l1+2)/(2*l1+3)*(l1+2)/2/2;     % f_(l+1)(k)  
                        A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) = +qe*E0*(l1-1)/(2*l1-1)*ue(k1)/Du ...
                                                              -qe*E0*(l1-1)/(2*l1-1)*(l1-1)/2/2;  % f_(l-1)(k)
                    elseif k1 == k2 + 1
                        A_f1T(k1+(l1-1)*Nu,k2+Nu+(l1-1)*Nu) = -qe*E0*(l1+2)/(2*l1+3)*ue(k1)/Du ...
                                                              +qe*E0*(l1+2)/(2*l1+3)*(l1+2)/2/2; % f_(l+1)(k-1)
                        A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) = -qe*E0*(l1-1)/(2*l1-1)*ue(k1)/Du ...
                                                              -qe*E0*(l1-1)/(2*l1-1)*(l1-1)/2/2;    % f_(l-1)(k-1)                                
                    end
                else % l1 ='odd'
                    if k1 == k2 
                        for Ng = 1:N_gases
                            A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) = A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) ...
                                                              +frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                        end
                        A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) =  A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                        A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) =  -qe*E0*(l1-1)/(2*l1-1)*uo(k1)/Du ...
                                                               -qe*E0*(l1-1)/(2*l1-1)*(l1-1)/2/2;      % f_(l-1)(k)
                        A_f1T(k1+(l1-1)*Nu,k2+Nu+(l1-1)*Nu) =  -qe*E0*(l1+2)/(2*l1+3)*uo(k1)/Du ...
                                                               +qe*E0*(l1+2)/(2*l1+3)*(l1+2)/2/2;  % f_(l+1)(k)
                    elseif k1 == k2 - 1
                        A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) = +qe*E0*(l1-1)/(2*l1-1)*uo(k1)/Du ...
                                                              -qe*E0*(l1-1)/(2*l1-1)*(l1-1)/2/2;       % f_(l-1)(k+1)
                        A_f1T(k1+(l1-1)*Nu,k2+Nu+(l1-1)*Nu) = +qe*E0*(l1+2)/(2*l1+3)*uo(k1)/Du ...
                                                              +qe*E0*(l1+2)/(2*l1+3)*(l1+2)/2/2;   % f_(l+1)(k+1)
                    end
                end
            elseif  l1 == N_terms-1 % l1='odd'
                if k1 == k2 
                    for Ng = 1:N_gases
                        A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) = A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) ...
                                                          +frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                    end
                    A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) =  A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                    A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) = -qe*E0*(l1-1)/(2*l1-1)*uo(k1)/Du ...
                                                          -qe*E0*(l1-1)/(2*l1-1)*(l1-1)/2/2;  % f_(l-1)(k)
                elseif k1 == k2 - 1
                    A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) = 0;       % f_l(k+1)
                    A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) =+qe*E0*(l1-1)/(2*l1-1)*uo(k1)/Du ...
                                                         -qe*E0*(l1-1)/(2*l1-1)*(l1-1)/2/2;     % f_(l-1)(k+1)
                elseif k1 == k2 + 1
                    A_f1T(k1+(l1-1)*Nu,k2+(l1-1)*Nu) = 0;     % f_l(k-1)
                    A_f1T(k1+(l1-1)*Nu,k2-Nu+(l1-1)*Nu) = 0;  % f_(l-1)(k-1)
                end
            end
        end
    end
end

b_f1T(1:Nu*(N_terms-1)) = 0;
for l1 = 1:N_terms-1
   for k1 = 1:Nu 
       if 1 <= l1 && l1 <= N_terms-2
           if mod(l1,2) == 0 % l1='even'
               if k1 ~= 1
                   b_f1T(k1+(l1-1)*Nu) = + 1/(2*l1-1)*ue(k1)*(x_f(k1+l1*Nu-Nu)+x_f(k1+l1*Nu-Nu-1))/2 - 1/(2*l1+3)*ue(k1)*(x_f(k1+l1*Nu+Nu)+x_f(k1+l1*Nu+Nu-1))/2;
               else
                   b_f1T(k1+(l1-1)*Nu) = + 1/(2*l1-1)*ue(k1)*x_f(k1+l1*Nu-Nu)/2 - 1/(2*l1+3)*ue(k1)*x_f(k1+l1*Nu+Nu)/2;
               end
           else
               if k1 ~= Nu
                   b_f1T(k1+(l1-1)*Nu) = + 1/(2*l1-1)*uo(k1)*(x_f(k1+l1*Nu-Nu)+x_f(k1+l1*Nu-Nu+1))/2 - 1/(2*l1+3)*uo(k1)*(x_f(k1+l1*Nu+Nu)+x_f(k1+l1*Nu+Nu+1))/2;
               else
                   b_f1T(k1+(l1-1)*Nu) = + 1/(2*l1-1)*uo(k1)*x_f(k1+l1*Nu-Nu)/2 - 1/(2*l1+3)*uo(k1)*x_f(k1+l1*Nu+Nu)/2;
               end
           end
       elseif l1 == N_terms-1
           if k1 ~= Nu
               b_f1T(k1+(l1-1)*Nu) = + 1/(2*l1-1)*uo(k1)*(x_f(k1+l1*Nu-Nu)+x_f(k1+l1*Nu-Nu+1))/2;
           else
               b_f1T(k1+(l1-1)*Nu) = + 1/(2*l1-1)*uo(k1)*x_f(k1+l1*Nu-Nu)/2;
           end
       end
   end
end
b_f1T = b_f1T*Np;

% Scale the matrix to avoid machine precision problems
b_f1T = b_f1T/max(max(A_f1T));
A_f1T = A_f1T/max(max(A_f1T));

A_f1T = sparse(A_f1T);
x_f1T = (A_f1T\(b_f1T')); % Solve the system
f1T_1(1:Nu) = x_f1T(1:Nu);
DFT_N = 1/3*sqrt(2/me/qe)*trapz(uo/qe,uo.*f1T_1); % Flux portion of DT_N
