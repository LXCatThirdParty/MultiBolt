// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// Finite difference grid
// grids are created only within 'solve', not created by the user

// for now: a grid object is what actually owns the grid parameters
class Grid {

public:

	mb::GridStyle grid_style;

	arma::uvec ell0_idx; // indices of x which are for ell = 0
	arma::uvec ell1_idx; // indices of x which are for ell = 1

	arma::uvec ell2_idx; // indices of x which are for ell = 2

	arma::uvec idx; // all 0,1...Nu-1 grid indices
	arma::uvec t_idx; // truncated once from the head
	arma::uvec idx_t; // truncated once from the tail
	arma::uvec t_idx_t; // truncated once from both ends


	arma::colvec u_e; // even J grid
	arma::colvec u_o; // odd J grid

	arma::colvec eV_e;
	arma::colvec eV_o;

	//arma::sword EVEN = 0;
	//arma::sword ODD = 1;

	//double Du; // finite uniform grid difference in J

	int N_collisions;
	int N_species;

	arma::mat sigma_e; // gridded collisions (even) // sigma always stands for a cross section
	arma::mat sigma_o; // gridded collisions (odd)

	arma::field< std::shared_ptr<lib::AbstractXsec>> xsec_ptr;
	arma::field< std::shared_ptr<lib::Species>> species_ptr;

	arma::uvec i_collision; // 0, 1, 2, etc

	arma::uvec i_species; // 0, 1, 2, etc based on species (different range!)

	arma::uvec i_collision_species; // collision belongs to the ith species 

	arma::colvec i_frac; // frac-presence of species for the ith collision


	arma::Col<int> i_codes;

	arma::uvec i_effective;
	arma::uvec i_elastic;
	arma::uvec i_excitation;
	arma::uvec i_ionization;
	arma::uvec i_attachment;
	arma::uvec i_superelastic;

	
};




