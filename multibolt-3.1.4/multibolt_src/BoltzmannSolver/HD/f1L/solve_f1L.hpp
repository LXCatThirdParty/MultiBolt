// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// solve governing equation for 1L gradient
void mb::BoltzmannSolver::solve_f1L() {

	mb::debug_statement("Begin solving governing equation for f1L.");

	// initial guess for omega1
	omega1 = calculate_W_f0();

	double omega1_prev = arma::datum::nan;

	//double Gamma_f1L = omega1;

	arma::sword N = present_Nu * p.N_terms;

	arma::Col<double> b(N, arma::fill::zeros);	// solve-against vector (contains normalization condition)
	arma::SpMat<double> A(N, N);				// coefficient matrix

	bool CONVERGED = false;

	for (iter_f1L = 1; iter_f1L <= p.iter_min || (iter_f1L < p.iter_max && !CONVERGED); ++iter_f1L) {

		A.zeros();
		b.zeros();

		for (arma::sword ell = 0; ell < p.N_terms; ++ell) {
			gov_eq_f1L(ell, A, b);
		}
		A = A - A_scattering;

		double norm = arma::max(arma::max(A));
		A = A / norm;
		b = b / norm;

		// normalization for f1L
		A.row(present_Nu - 1).zeros();
		A.head_cols(present_Nu).row(present_Nu -  1) = (sqrt(g.u_e / mb::QE) % Du_at_u(g.u_e) / mb::QE).t();
		b(present_Nu - 1) = 0;

		// - scheme insensitive to BC
		//// Boundary conditions
		//for (arma::sword ell = 0; ell < p.N_terms; ++ell) {
		//	boundary_conditions(ell, A, b);
		//}




		bool solve_success = arma::spsolve(x_f1L, A, b, "superlu", multibolt_superlu_opts()); // solve
		if (!solve_success) {
			mb::error_statement("Solution failing: spsolve unsuccesful.");
			this->solution_succesful = false;
			return;
		}
		// fllux portion of longitudinal diffusion
		//DFLN = calculate_DFLN(); 

		
		omega1_prev = omega1;

		//double S_1L = (calculate_total_S(x_f1L(g.ell0_idx), lib::CollisionCode::ionization) - calculate_total_S(x_f1L(g.ell0_idx), lib::CollisionCode::attachment));
		//omega1 = Gamma_f1L + S_1L;

		omega1 = calculate_W_BULK();
		



		// check if ionization is ocurring at all - converge against avg_en instead if need be
		if (this->NO_ITERATION || omega0 == 0) {
			mb::debug_statement("Single iteration case: no ionization or attachment found.");
			mb::display_iteration_banner("f1L", iter_f1L, "omega1 [m s^-1]", omega1, 0);
			CONVERGED = true;
			break;
		}
		else {
			mb::display_iteration_banner("f1L", iter_f1L, "omega1 [m s^-1]", omega1, err(omega1, omega1_prev));
			CONVERGED = mb::converged(p.conv_err, omega1, omega1_prev);
		}

		// extra check: assume that if you are past iter_min, you're willing to let SHYness boot the solution out early
		if (iter_f1L > p.iter_min) {
			if (this->check_is_solution_failing(1) == true) {
				return;
			}
		}
	}

	mb::check_did_not_converge("f1L", iter_f1L, p.iter_max);

	mb::debug_statement("Exit solving governing equation for f1L.");

}
