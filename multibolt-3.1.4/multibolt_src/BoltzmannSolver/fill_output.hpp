// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// set aside the space which you expect the output elements to need
void mb::BoltzmannSolver::carve_output() {


	for (auto spec : lib.allspecies) {

		mb::BoltzmannOutputPerGas this_pergas;

		this_pergas.k_ela_N = arma::vec(spec->n_ela(), arma::fill::zeros);
		this_pergas.k_eff_N = arma::vec(spec->n_eff(), arma::fill::zeros);

		this_pergas.k_exc_N = arma::vec(spec->n_exc(), arma::fill::zeros);
		this_pergas.k_iz_N = arma::vec(spec->n_iz(), arma::fill::zeros);
		this_pergas.k_att_N = arma::vec(spec->n_att(), arma::fill::zeros);


		this_pergas.alpha_N = arma::vec(spec->n_iz(), arma::fill::zeros);
		this_pergas.eta_N = arma::vec(spec->n_att(), arma::fill::zeros);


		this_pergas.k_sup_N = arma::vec(spec->n_sup(), arma::fill::zeros);

		out.per_gas.push_back(this_pergas);

	}


	out.eV_even = arma::colvec(present_Nu, arma::fill::zeros);
	out.eV_odd = arma::colvec(present_Nu, arma::fill::zeros);

	
}




void mb::BoltzmannSolver::fill_output() {

	
	
	
	out.p = p;

	if (this->solution_succesful == false) { // setting all distr-functions to nan-filled 

		this->x_f = arma::colvec(present_Nu * 2, arma::fill::value(arma::datum::nan));
		
		this->x_f1L = arma::colvec(present_Nu * 2, arma::fill::value(arma::datum::nan));
		this->x_f1T = arma::colvec(present_Nu * 2, arma::fill::value(arma::datum::nan));
		this->x_f2L = arma::colvec(present_Nu * 2, arma::fill::value(arma::datum::nan));
		this->x_f2T = arma::colvec(present_Nu * 2, arma::fill::value(arma::datum::nan));

	}

	// Fill general elements
	out.eV_even = g.u_e / mb::QE;
	out.eV_odd = g.u_o / mb::QE;

	out.f0 = x_f(g.ell0_idx);
	out.f1 = x_f(g.ell1_idx);

	out.eV_max = present_eV_max;
	
	out.Nu = present_Nu;

	out.time = calculation_time;

	out.avg_en = calculate_avg_en();
	out.D_mu = calculate_D_mu();
	out.DT_mu = calculate_DT_mu();
	out.DL_mu = calculate_DL_mu();

	out.DN_f0 = calculate_DN_f0();
	out.DN_f0_nu = calculate_DN_f0_nu();

	out.energy_DN_f0 = calculate_energy_DN_f0();
	out.energy_DN_f0_nu = calculate_energy_DN_f0_nu();


	out.muN_SST = calculate_muN_f0();

	out.muN_FLUX = calculate_muN_f0();

	out.muN_BULK = calculate_muN_BULK();

	out.energy_muN = calculate_energy_muN_f0();


	out.k_iz_eff_N = calculate_k_iz_eff_N();

	out.alpha_eff_N = calculate_alpha_eff_N();

	out.W_SST = (p.model == mb::ModelCode::SST) ? calculate_W_f0() : arma::datum::nan;


	// HD+Gradient-expansion dependent
	out.W_FLUX = (p.model == mb::ModelCode::HD || p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) ? calculate_W_f0() : arma::datum::nan;
	out.W_BULK = calculate_W_BULK();

	out.DTN_FLUX = calculate_DFTN();
	out.DLN_FLUX = calculate_DFLN();

	out.DLN_BULK = calculate_DLN_BULK();
	out.DTN_BULK = calculate_DTN_BULK();


	// Fill 'total' elements
	out.total.k_ela_N = calculate_total_rate(g.i_elastic);
	out.total.k_eff_N = calculate_total_rate(g.i_effective);
	out.total.k_exc_N = calculate_total_rate(g.i_excitation);
	out.total.k_iz_N = calculate_total_rate(g.i_ionization);
	out.total.k_att_N = calculate_total_rate(g.i_attachment);
	out.total.k_sup_N = calculate_total_rate(g.i_superelastic);

	// SST conditions only
	out.total.alpha_N = calculate_total_growth(g.i_ionization);
	out.total.eta_N = calculate_total_growth(g.i_attachment);



	arma::vec rates;
	int Ng = 0;
	int i = 0;

	// Fill each per-gas element
	for (auto i_spec : g.i_species) {
	
		auto spec = g.species_ptr(i_spec);

		out.per_gas[i_spec].name = spec->name();

		i = 0;
		for (auto x : spec->ela) {
			out.per_gas[Ng].k_ela_N(i) = calculate_rate(x);
			i++;
		}

		i = 0;
		for (auto x : spec->eff) {
			out.per_gas[Ng].k_eff_N(i) = calculate_rate(x);
			i++;
		}

		i = 0;
		for (auto x : spec->exc) {
			out.per_gas[Ng].k_exc_N(i) = calculate_rate(x);
			i++;
		}

		i = 0;
		for (auto x : spec->iz) {
			out.per_gas[Ng].k_iz_N(i) = calculate_rate(x);
			out.per_gas[Ng].alpha_N(i) = calculate_growth(x);
			i++;
		}

		i = 0;
		for (auto x : spec->att) {
			out.per_gas[Ng].k_att_N(i) = calculate_rate(x);
			out.per_gas[Ng].eta_N(i) = calculate_growth(x);
			i++;
		}

		i = 0;
		for (auto x : spec->sup) {
			out.per_gas[Ng].k_sup_N(i) = calculate_rate(x);
			i++;
		}

		Ng++;
	}



}