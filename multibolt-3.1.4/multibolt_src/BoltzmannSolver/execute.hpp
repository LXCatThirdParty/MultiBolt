// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

// 08/2023: Special thanks to Cameron Wagoner for contributing an Nu remap scheme.

#pragma once

#include "multibolt"

// -- Perform MultiBolt solution 
void mb::BoltzmannSolver::execute() {


	this->x_f = arma::colvec(present_Nu * p.N_terms, arma::fill::zeros);
	this->present_weight_f0 = p.weight_f0;
	set_grid(); // initial grid-set. Also fills A_scattering. These two only change per-f0=calculation if the grid is allowed to update.

	bool GRIDMAX_HAS_CHANGED = false;
	bool NU_HAS_CHANGED = false;

	bool NO_MORE_REMAP = false;

	for (int i = 0; i < p.remap_grid_trial_max + 1; ++i) {

		if (!GRIDMAX_HAS_CHANGED && !NU_HAS_CHANGED && (i > 0)) { // ignore on first-go
			mb::normal_statement("No more remap, further remap would not change solution.");
			NO_MORE_REMAP = true;
			break;
		}
		if (i == (p.remap_grid_trial_max - 1)) {
			mb::normal_statement("No more remap, reached remap trial max.");
			NO_MORE_REMAP = true;
			break;
		}


	    if (p.model == mb::ModelCode::HD || p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) {
	        solve_f0_HD();
	    }
	    else if (p.model == mb::ModelCode::SST) {
	        solve_f0_SST();
	    }


	    
	    // begin grid-max remap section
		if (p.USE_ENERGY_REMAP && !NO_MORE_REMAP) {
			GRIDMAX_HAS_CHANGED = false; 
			present_weight_f0 = p.weight_f0; // necessary!

	        mb::RemapChoice op = check_grid_max();
	        if (op == mb::RemapChoice::LowerGridMax) {
		        mb::normal_statement("Tail of f0 looks small; grid maximum is being lowered. f0 will be solved again.");
		        lower_grid();
				GRIDMAX_HAS_CHANGED = true;
				continue;
	        }
			else if (op == mb::RemapChoice::RaiseGridMaxLots) {
				mb::normal_statement("Tail of f0 looks very large; grid minimum is being raised significantly. f0 will be solved again.");
				//raise_grid_max_extrap();
				raise_grid_max_lots();
				GRIDMAX_HAS_CHANGED = true;
				continue;
			}
			else if (op == mb::RemapChoice::RaiseGridMaxLittle) {
				mb::normal_statement("Tail of f0 looks large; grid minimum is being raised a bit. f0 will be solved again.");
				//raise_grid_max_extrap(); // <- unintended behavior. Way too aggressive.

				raise_grid_max_little();

				GRIDMAX_HAS_CHANGED = true;
				continue;
			}
			//else {
				// grid is fine, can be left alone
				//mb::normal_statement("Grid-max is acceptable and will not be adjusted.");
			//}
			
	        
	    }
	    

		// begin Nu remap section
		// 08/2023 Adapted from code first given by Cameron Wagoner. Thanks!
		if (p.USE_NU_REMAP == true) {
			NU_HAS_CHANGED = false;
			present_weight_f0 = p.weight_f0; // necessary!

			// Current indicator is to check if any f0 elements are negative
			double least = min(x_f(g.ell0_idx));
			double fnorm = check_f0_normalization();
			if (least < 0 || (fnorm > 1.1 || fnorm < 0.9)) {
				mb::normal_statement("Attempting to improve f0 by increasing Nu.");
				if (present_Nu < p.remap_Nu_max) {
					present_Nu = std::min((present_Nu + p.remap_Nu_increment), p.remap_Nu_max);
					mb::normal_statement("Nu was raised to " + mb::mb_format(present_Nu) + ".");
					set_grid();
					NU_HAS_CHANGED = true;
				}
				else {
					mb::normal_statement("Nu is large, and will not be increased further.");
					NU_HAS_CHANGED = false;
				}
			}	
		}

		
	}

	if (!this->solution_succesful) {
		mb::normal_statement("Solution failed, exiting.");
		return; // exit out because results beyond this will be unrealistic
	}

		

	// No particular reason you can't use GE with only 2 terms
	// But it tends to whiff on the diffusion coefficients
	if (p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) {

		this->x_f1L = arma::colvec(present_Nu * p.N_terms, arma::fill::value(arma::datum::nan));
		this->x_f1T = arma::colvec(present_Nu * p.N_terms, arma::fill::value(arma::datum::nan));
		this->x_f2L = arma::colvec(present_Nu * p.N_terms, arma::fill::value(arma::datum::nan));
		this->x_f2T = arma::colvec(present_Nu * p.N_terms, arma::fill::value(arma::datum::nan));

	#ifdef MULTIBOLT_USING_OPENMP

		#pragma omp parallel sections
		{

			#pragma omp section
			solve_f1T();

			#pragma omp section
			solve_f1L();
		}

		if (!this->solution_succesful) {
			mb::normal_statement("Solution failed, exiting.");
			return; // exit out because results beyond this will be unrealistic
		}


		if (p.model == mb::ModelCode::HDGE) {
			// Calculate second set of gradient expansion systems
			
			#pragma omp parallel sections
			{

				#pragma omp section
				solve_f2T();

				#pragma omp section
				solve_f2L();

			}

			if (!this->solution_succesful) {
				mb::normal_statement("Solution failed, exiting.");
				return; // exit out because results beyond this will be unrealistic
			}
		}

	#else

		// totally without openmp.
		solve_f1T();
		solve_f1L();

		if(!this->solution_succesful) {
			mb::normal_statement("Solution failed, exiting.");
			return; // exit out because results beyond this will be unrealistic
		}

		if (p.model == mb::ModelCode::HDGE) {
			solve_f2T();
			solve_f2L();
		}

		if (!this->solution_succesful) {
			mb::normal_statement("Solution failed, exiting.");
			return; // exit out because results beyond this will be unrealistic
		}

	#endif


	}

}
