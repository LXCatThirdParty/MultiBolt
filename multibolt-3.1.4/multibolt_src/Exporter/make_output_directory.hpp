// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

void mb::Exporter::make_output_directory() {

	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;

	// handle default name of general export directory
	if (export_location.compare(mb::UNALLOCATED_STRING) == 0) {
		export_location = fs::path(fs::current_path().string() + mb::sep() + mb::DEFAULT_EXPORT_PATH).make_preferred().lexically_normal().string();
	}

	// if directory does not already exist, create it
	if (!fs::exists(export_location)) {
		fs::create_directory(export_location);
		mb::normal_statement("Created the directory '" + export_location + "'.");
	}

	// handle default name of data directory (+1 per folder)
	if (export_name.compare(mb::UNALLOCATED_STRING) == 0 && sweep_idx == 0) {

		// count folders already inside this dir
		int count = 0;
		for (auto& p : fs::directory_iterator(export_location)) {

			if (fs::is_directory(p)) {
				count++;
			}
		}

		do {

			// theres a chance this dir already exists. if it does, keep counting up until it doesn't.
			export_name = "Run_" + std::to_string(count);
			count++;

		} while (fs::exists(fs::path(export_location + mb::sep() + export_name).make_preferred().lexically_normal()));

	}

	this->full_path = fs::path(export_location + mb::sep() + export_name).make_preferred().lexically_normal();


	if (fs::exists(this->full_path)) {
		mb::normal_statement("Custom output name already exists, and will be overridden.");
		// delete all directory contents
		fs::remove_all(this->full_path);
	}

	fs::create_directory(this->full_path);

	mb::normal_statement("Created output location: '" + export_name + "' in directory: '" + export_location + "' ");



}