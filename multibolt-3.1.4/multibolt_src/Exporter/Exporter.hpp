// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// Todo: whole class could use an overhaul

// The class which, given a multibolt output data struct, will export and print to files.
class Exporter {

	bool export_limited = false; // flag

	// each 'exportable' corresponds to something that you want to be output
	// to add or remove one, look in 'exportables' folder

	struct exportable {
		std::string filename = mb::UNALLOCATED_STRING;
		std::vector<std::string> headerlines = { mb::UNALLOCATED_STRING };
		std::string dir = mb::UNALLOCATED_STRING;
		std::string key1 = mb::UNALLOCATED_STRING;
		std::string key2 = mb::UNALLOCATED_STRING;
		double x = arma::datum::nan;
		double y = arma::datum::nan;
	};

	struct exportable_f {
		std::string filename = mb::UNALLOCATED_STRING;
		std::vector<std::string> headerlines = { mb::UNALLOCATED_STRING };
		std::string dir = mb::UNALLOCATED_STRING;
		std::string key1 = mb::UNALLOCATED_STRING;
		std::string key2 = mb::UNALLOCATED_STRING;
		arma::colvec leftcol; // matrix for eedf
		arma::colvec rightcol;
	};


private:

	std::string export_location;
	std::string export_name;
	mb::BoltzmannParameters p;
	mb::BoltzmannOutput out;
	std::filesystem::path full_path;
	lib::Library lib;

	

	mb::sweep_option sweep_op;


	std::string col1_var;
	std::string col1_units;
	double col1_val;



	std::vector<exportable> exportables;
	std::vector<exportable_f> perRun_exportables;


public:

	int sweep_idx;

	Exporter() {
		// default constructor, set nothing?
	}

	// Export as if its a swept run
	// the run-details will have a tag of <SWEEP> replacing the value of the tagged variable
	// and the first-column might be something other than EN_Td
	Exporter(const lib::Library& lib, const mb::BoltzmannParameters& p, const mb::BoltzmannOutput& out, 
		const std::string& export_location, const std::string& export_name, mb::sweep_option sweep_op, bool export_xsecs=false, bool export_limited=false) {
		


		namespace fs = std::filesystem;

		this->export_limited = export_limited;

		this->p = p;
		this->out = out;

		// correct export-procedure here if the user is trying to write to folders that cannot exist (invalid chars, reserved names, etc)
		// do not quit out to avoid wasting a users time
			bool valid_export_location = mb::is_valid_dirstring(export_location);
			bool valid_export_name = mb::is_valid_dirstring(export_name);

			if (!valid_export_location) {
				mb::normal_statement("Warning: Export location '" + export_location + "' cannot be written to on this machine. Default location will be used instead.");
				this->export_location = mb::UNALLOCATED_STRING;
			}
			else {
				this->export_location = export_location;
			}


			if (!valid_export_name) {
				mb::normal_statement("Warning: Export name '" + export_name + "' is not a string which can be a valid directory on this machine. Default convention will be used instead.");
				this->export_name = mb::UNALLOCATED_STRING;
			}
			else {
				this->export_name = export_name;
			}

			


		this->lib = lib;

		this->sweep_idx = 0;

		this->sweep_op = sweep_op;

		pick_based_on_sweep();

		make_output_directory();

		make_file_hierarchy();

		make_run_details();
		make_species_indices();
		make_collision_indices();

		create_toplevel_exportables();
		instantiate_toplevel_txts();

		if (export_xsecs) {
			print_xsecs(); // important: should only do once, on instantiation
		}

	}


	public:
		std::string get_export_location() { return export_location; }
		std::string get_export_name() { return export_name; };
		void write_this_run(const mb::BoltzmannParameters& p, const mb::BoltzmannOutput& out);


	private:
		void make_output_directory();
		void make_file_hierarchy(); ///////
		void make_run_details();
		void pick_based_on_sweep();

		void create_toplevel_exportables();
		void create_perRun_exportables();

		void instantiate_toplevel_txts();
		void instantiate_perRun_txts();

		void make_species_indices();
		void make_collision_indices();

		void print_xsecs();
};







// handles sweep_idx
void mb::Exporter::write_this_run(const mb::BoltzmannParameters& p, const mb::BoltzmannOutput& out) {


	this->p = p;
	this->out = out;

	pick_based_on_sweep();

	create_toplevel_exportables();

	create_perRun_exportables(); // this does need to be done per-run

	instantiate_perRun_txts();

	for (auto& exp : exportables) {
		mb::append_to_txt(exp.dir, exp.filename, arma::colvec({ exp.x }), arma::colvec({ exp.y }));
	}

	for (auto& exp : perRun_exportables) {
		mb::append_to_txt(exp.dir, exp.filename,  exp.leftcol, exp.rightcol );
	}


	mb::debug_statement("Data written with sweep_idx: "+ sweep_idx);

	sweep_idx = sweep_idx + 1;

	return;
}




// print xsecs to directory structure based on the lib
void mb::Exporter::print_xsecs() {

	// make file hierarchy of the xsecs, similar to PerGas structure

	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;

	fs::create_directory(fs::path(full_path.string() + mb::sep() + "Xsecs").make_preferred().lexically_normal());


	//std::vector<fs::path> dirs;
	// Add in a folder per-gas
	for (int Ng = 0; Ng < lib.allspecies.size(); ++Ng) {
		auto thispath = fs::path(full_path.string() + mb::sep() + "Xsecs" + mb::sep() + "Gas_" + std::to_string(Ng)).make_preferred().lexically_normal();
	

		fs::create_directory(thispath.string());

		int c_att = 0, c_ela = 0, c_eff = 0, c_exc = 0, c_iz = 0, c_sup = 0;

		for (auto& x : lib.allspecies.at(Ng)->allcollisions) {

			


				std::string thisname = "";

				switch (x->code()) {

				case lib::CollisionCode::attachment:
					thisname = "att_" + std::to_string(c_att);
					c_att++;
					break;
				case lib::CollisionCode::elastic:
					thisname = "ela_" + std::to_string(c_ela);
					c_ela++;
					break;
				case lib::CollisionCode::effective:
					thisname = "eff_" + std::to_string(c_eff);
					c_eff++;
					break;
				case lib::CollisionCode::excitation:
					thisname = "exc_" + std::to_string(c_exc);
					c_exc++;
					break;
				case lib::CollisionCode::ionization:
					thisname = "iz_" + std::to_string(c_iz);
					c_iz++;
					break;
				case lib::CollisionCode::superelastic:
					thisname = "sup_" + std::to_string(c_sup);
					c_sup++;
					break;

				}

				std::vector<std::string> headerlines = { "The cross section evaluated in the same manner as is done inside MultiBolt",
					"Reactant: " + x->reactant(),
					"Product: " + x->product(),
					"Process: " + x->process(),
					"col_1: eV [eV], col_2: s [m^2]" };

				mb::init_txt(thispath.string(), thisname, headerlines, "eV", "s");
			

				if (x->code() == lib::CollisionCode::ionization || x->code() == lib::CollisionCode::excitation 
					|| x->code() == lib::CollisionCode::rotational || x->code() == lib::CollisionCode::vibrational) {
					arma::colvec eV = arma::logspace(std::log10(x->eV_thresh()), 4 + std::log10(x->eV_thresh()), 400);
					mb::append_to_txt(thispath.string(), thisname, eV, x->eval_at_eV(eV, p.interp_method));
				}
				else{
					arma::colvec eV = arma::logspace(-4, +4, 400);
					mb::append_to_txt(thispath.string(), thisname, eV, x->eval_at_eV(eV, p.interp_method));
				}
				
		}
	}

}

