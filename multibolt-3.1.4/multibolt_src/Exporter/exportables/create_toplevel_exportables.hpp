// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

// TODO: whole file probably belongs to Exporter project


// if you want MultiBolt to export more things per-run (toplevel only), push_back an object here
void mb::Exporter::create_toplevel_exportables() {

	exportables = {}; // reset


	std::string TOP_LEVEL = full_path.string() + mb::sep() + "." + mb::sep();
	std::string TOTAL_LEVEL = full_path.string() + mb::sep() + "Total" + mb::sep();


	std::vector<std::string> GAS_LEVELS = {};
	for (int Ng = 0; Ng < out.per_gas.size(); ++Ng) {

		GAS_LEVELS.push_back(full_path.string() + mb::sep() + "PerGas" +  mb::sep() + "Gas_" + std::to_string(Ng));
	}


	// Form to use:
	/*

	exportable{
		filename		- string, extension-less identifier by filename
		hearderlines	- vector<string>, text which will be preceded by # signs to be headers
		dir				- string, the relative path the txt will exist at
		key1			- string, the variable name for the first column
		key2			- string, the variable name for the second column
		x				- double, the number in the first column
		y				- double, the number in the second column
		}
	*/

	// I intend that in order to add or subtract any outputtable features, you only
	// have to change anything in *this* function



	if (this->export_limited == true) {
		// in limited: only four special print here

		// k_iz_eff_N ------------------------------------
		this->exportables.push_back(
			exportable{
				"k_iz_eff_N",
				{{"The density-reduced effective (net) ionization collision rate coefficient."},
					{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_iz_eff_N [m**3 s^-1]"} },
				TOP_LEVEL,
				col1_var,
				"k_iz_eff_N",
				col1_val,
				out.k_iz_eff_N }
		);

		// W_BULK ------------------------------------
		this->exportables.push_back(
			exportable{
				"W_BULK",
				{{"(HD+GE only) The bulk drift velocity."},
					{ "col_1: " + col1_var + " " + col1_units + ", col_2: W_BULK [m s^-1]" }},
				TOP_LEVEL,
				col1_var,
				"w_BULK",
				col1_val,
				out.W_BULK }
		);

		// DLN_BULK ------------------------------------
		this->exportables.push_back(
			exportable{
				"DLN_BULK",
				{{"(HD+GE only) The bulk longitudinal diffusion coefficient density-product."},
					{ "col_1: " + col1_var + " " + col1_units + ", col_2: DLN_BULK [m^-1 s^-1]" }},
				TOP_LEVEL,
				col1_var,
				"DLN_BULK",
				col1_val,
				out.DLN_BULK }
		);

		// alpha_eff_N -------------------------------------
		this->exportables.push_back(
			exportable{
				"alpha_eff_N",
				{ {"The density-reduced effective (net) ionization growth coefficient."},
					{"Also known as the 'first Townsend coefficient' or 'primary ionization coefficient'"},
					{"col_1: " + col1_var + " " + col1_units + ", col_2: alpha_eff_N [m**2]"} },
				TOP_LEVEL,
				col1_var,
				"alpha_eff_N",
				col1_val,
				out.alpha_eff_N }
		);

		return;
	}
	







	// sweep ------------------------------------
	// BoltzmannOutput sweep indices vs dependent var
	this->exportables.push_back(
		exportable{
			"sweep_indices",
			{{"The indices of particular swept values."}, {"Sweep is denoted per E_N by default."},
				{"col_1: sweep_idx [count], col_2: " + col1_var + " " + col1_units }},
			TOP_LEVEL,
			"sweep_idx",
			col1_var,
			floor(double(sweep_idx)), // make sure it stays int-like
			col1_val }
	);

	

	// time ------------------------------------
	this->exportables.push_back(
		exportable{
			"time",
			{{"The real time spent on calculation."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: time [s]"} },
			TOP_LEVEL,
			col1_var,
			"time",
			col1_val,
			out.time }
	);

	// avg_en ------------------------------------
	this->exportables.push_back(
		exportable{
			"avg_en",
			{{"The average (mean) electron energy."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: avg_en [eV]" }},
			TOP_LEVEL,
			col1_var,
			"avg_en",
			col1_val,
			out.avg_en }
	);

	// Characteristic energy D_mu ------------------------------------
	this->exportables.push_back(
		exportable{
			"D_mu",
			{{"The characteristic electron energy (based on f0 only)."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: D_mu [eV]" }},
			TOP_LEVEL,
			col1_var,
			"D_mu",
			col1_val,
			out.D_mu }
	);

	// Characteristic energy DT_mu ------------------------------------
	this->exportables.push_back(
		exportable{
			"DT_mu",
			{{"The characteristic electron energy (based on fT only)."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DT_mu [eV]" }},
			TOP_LEVEL,
			col1_var,
			"DT_mu",
			col1_val,
			out.DT_mu }
	);

	// Characteristic energy DT_mu ------------------------------------
	this->exportables.push_back(
		exportable{
			"DL_mu",
			{{"The characteristic electron energy (based on fL only)."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DL_mu [eV]" }},
			TOP_LEVEL,
			col1_var,
			"DL_mu",
			col1_val,
			out.DL_mu }
	);



	// alpha_eff_N ------------------------------------
	this->exportables.push_back(
		exportable{
			"alpha_eff_N",
			{ {"The density-reduced effective (net) ionization growth coefficient."},
				{"Also known as the 'first Townsend coefficient' or 'primary ionization coefficient'"},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: alpha_eff_N [m**2]"} },
			TOP_LEVEL,
			col1_var,
			"alpha_eff_N",
			col1_val,
			out.alpha_eff_N }
	);


	// k_iz_eff_N ------------------------------------
	this->exportables.push_back(
		exportable{
			"k_iz_eff_N",
			{{"The density-reduced effective (net) ionization collision rate coefficient."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_iz_eff_N [m**3 s^-1]"} },
			TOP_LEVEL,
			col1_var,
			"k_iz_eff_N",
			col1_val,
			out.k_iz_eff_N }
	);

	// muN_SST ------------------------------------
	this->exportables.push_back(
		exportable{
			"muN_SST",
			{{"(SST only) The electron mobility density-product per SST conditions."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: muN_SST [m^-1 s^-1 V^-1]"} },
			TOP_LEVEL,
			col1_var,
			"muN_SST",
			col1_val,
			out.muN_SST }
	);

	// muN_FLUX ------------------------------------
	this->exportables.push_back(
		exportable{
			"muN_FLUX",
			{{"(HD+GE only) The flux electron mobility density-product."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: muN_FLUX [m^-1 s^-1 V^-1]"} },
			TOP_LEVEL,
			col1_var,
			"muN_FLUX",
			col1_val,
			out.muN_FLUX }
	);

	// muN_BULK ------------------------------------
	this->exportables.push_back(
		exportable{
			"muN_BULK",
			{ {"(HD+GE only) The bulk electron mobility density-product."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: muN_BULK [m^-1 s^-1 V^-1]" }},
			TOP_LEVEL,
			col1_var,
			"muN_BULK",
			col1_val,
			out.muN_BULK }
	);

	// energy_muN_f0 ------------------------------------
	this->exportables.push_back(
		exportable{
			"energy_muN",
			{{"The energy mobility density-product."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: energy_muN [m^-1 s^-1 V^-1]"} },
			TOP_LEVEL,
			col1_var,
			"energy_muN",
			col1_val,
			out.energy_muN }
	);

	// W_BULK ------------------------------------
	this->exportables.push_back(
		exportable{
			"W_BULK",
			{{"(HD+GE only) The bulk drift velocity."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: W_BULK [m s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"w_BULK",
			col1_val,
			out.W_BULK }
	);

	// W_FLUX ------------------------------------
	this->exportables.push_back(
		exportable{
			"W_FLUX",
			{ {"(HD+GE only) The flux drift velocity."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: W_FLUX [m s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"W_FLUX",
			col1_val,
			out.W_FLUX }
	);

	// W_SST ------------------------------------
	this->exportables.push_back(
		exportable{
			"W_SST",
			{{"(SST only) The drift velocity of electrons per SST conditions."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: W_SST [m s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"W_SST",
			col1_val,
			out.W_SST }
	);

	// DLN_BULK ------------------------------------
	this->exportables.push_back(
		exportable{
			"DLN_BULK",
			{{"(HD+GE only) The bulk longitudinal diffusion coefficient density-product."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DLN_BULK [m^-1 s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"DLN_BULK",
			col1_val,
			out.DLN_BULK }
	);

	// DLN_FLUX ------------------------------------
	this->exportables.push_back(
		exportable{
			"DLN_FLUX",
			{{"(HD+GE only) The flux longitudinal diffusion coefficient density-product."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DLN_FLUX [m^-1 s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"DLN_FLUX",
			col1_val,
			out.DLN_FLUX }
	);

	// DTN_BULK ------------------------------------
	this->exportables.push_back(
		exportable{
			"DTN_BULK",
			{{"(HD+GE only) The bulk transverse diffusion coefficient density-product."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DTN_BULK [m^-1 s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"DTN_BULK",
			col1_val,
			out.DTN_BULK }
	);

	// DTN_FLUX ------------------------------------
	this->exportables.push_back(
		exportable{
			"DTN_FLUX",
			{{"(HD+GE only) The flux transverse diffusion coefficient density-product."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DTN_FLUX [m^-1 s^-1]" }},
			TOP_LEVEL,
			col1_var,
			"DTN_FLUX",
			col1_val,
			out.DTN_FLUX }
	);

	// DN_f0 ------------------------------------
	this->exportables.push_back(
		exportable{
			"DN_f0",
			{{"The conventional (isotropic) diffusion coefficient density-product for electrons."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: DN_f0 [m^-1 s^-1]"} },
			TOP_LEVEL,
			col1_var,
			"DN_f0",
			col1_val,
			out.DN_f0 }
	);

	// DN_f0_nu ------------------------------------
	this->exportables.push_back(
		exportable{
			"DN_f0_nu",
			{ {"The conventional (isotropic) diffusion coefficient density-product for neutrals."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: DN_f0_nu [m^-1 s^-1]"} },
			TOP_LEVEL,
			col1_var,
			"DN_f0_nu",
			col1_val,
			out.DN_f0_nu }
	);


	// energy_DN_f0 ------------------------------------
	this->exportables.push_back(
		exportable{
			"energy_DN_f0",
			{{"The conventional (isotropic) diffusion coefficient density-product for energy (does not include nonconservative effects)."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: energy_DN_f0 [m^-1 s^-1]"} },
			TOP_LEVEL,
			col1_var,
			"energy_DN_f0",
			col1_val,
			out.energy_DN_f0 }
	);

	// energy_DN_f0_nu ------------------------------------
	this->exportables.push_back(
		exportable{
			"energy_DN_f0_nu",
			{ {"The conventional (isotropic) diffusion coefficient density-product for energy (includes nonconservative effects)."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: energy_DN_f0_nu [m^-1 s^-1]"} },
			TOP_LEVEL,
			col1_var,
			"energy_DN_f0_nu",
			col1_val,
			out.energy_DN_f0_nu }
	);




	// eV_max ------------------------------------
	this->exportables.push_back(
		exportable{
			"eV_max",
			{ {"The maximum energy of the mesh at time of convergence."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: eV_max [eV]"} },
			TOP_LEVEL,
			col1_var,
			"eV_max",
			col1_val,
			out.eV_max }
	);

	// Nu (would change due to remap) ------------------------------------
	this->exportables.push_back(
		exportable{
			"Nu",
			{ {"The number of grid cells at time of convergence."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: Nu [a.u.]"} },
			TOP_LEVEL,
			col1_var,
			"Nu",
			col1_val,
			(double)out.Nu }
	);


	/// 
	/// Make those that go under 'Total' dir 
	/// 

	// k_ela_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"k_ela_N",
			{{"The density-reduced total elastic collision rate coefficient."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_ela_N [m**3 s^-1]"} },
			TOTAL_LEVEL,
			col1_var,
			"k_ela_N",
			col1_val,
			out.total.k_ela_N }
	);

	// k_eff_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"k_eff_N",
			{{"The density-reduced total effective collision rate coefficient."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_eff_N [m**3 s^-1]"} },
			TOTAL_LEVEL,
			col1_var,
			"k_eff_N",
			col1_val,
			out.total.k_eff_N }
	);

	// k_exc_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"k_exc_N",
			{{"The density-reduced total excitation collision rate coefficient."},
				{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_exc_N [m**3 s^-1]" }},
			TOTAL_LEVEL,
			col1_var,
			"k_exc_N",
			col1_val,
			out.total.k_exc_N }
	);

	// k_iz_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"k_iz_N",
			{ {"The density-reduced total ionization collision rate coefficient."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: k_iz_N [m**3 s^-1]" }},
			TOTAL_LEVEL,
			col1_var,
			"k_iz_N",
			col1_val,
			out.total.k_iz_N }
	);

	// k_att_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"k_att_N",
			{ {"The density-reduced total attachment collision rate coefficient."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: k_att_N [m**3 s^-1]" }},
			TOTAL_LEVEL,
			col1_var,
			"k_att_N",
			col1_val,
			out.total.k_att_N }
	);

	// k_sup_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"k_sup_N",
			{ {"The density-reduced total super-elastic collision rate coefficient."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: k_sup_N [m**3 s^-1]" }},
			TOTAL_LEVEL,
			col1_var,
			"k_sup_N",
			col1_val,
			out.total.k_sup_N }
	);


	// alpha_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"alpha_N",
			{ {"The density-reduced total ionization growth coefficient."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: alpha_N [m**2]" }},
			TOTAL_LEVEL,
			col1_var,
			"alpha_N",
			col1_val,
			out.total.alpha_N }
	);

	// eta_N ----------------------------------------
	this->exportables.push_back(
		exportable{
			"eta_N",
			{ {"The density-reduced total attachment growth coefficient."},
				{"col_1: " + col1_var + " " + col1_units + ", col_2: eta_N [m**2]" }},
			TOTAL_LEVEL,
			col1_var,
			"eta_N",
			col1_val,
			out.total.eta_N }
	);



	/// Now make all the docs for each particular gas and rate
	/// 
	for (auto it = lib.allspecies.begin(); it != lib.allspecies.end(); it++) {

		auto Ng = it - lib.allspecies.begin();

		auto spec = (*it);

		// Particular attachment rates
		for (auto it2 = spec->att.begin(); it2 != spec->att.end(); it2++){
			auto i = it2 - spec->att.begin();

			this->exportables.push_back(
				exportable{
					"k_att_N_" + std::to_string(i),
					{ {"The density-reduced particular attachment collision rate coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_att_N [m**3 s^-1]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"k_att_N",
					col1_val,
					out.per_gas[Ng].k_att_N[i] }
			);

			this->exportables.push_back(
				exportable{
					"eta_N_" + std::to_string(i),
					{ {"The density-reduced particular attachment growth coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_att_N [m**2]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"eta_N",
					col1_val,
					out.per_gas[Ng].eta_N[i] }
			);
		}

		// Particular elastic rates
		for (auto it2 = spec->ela.begin(); it2 != spec->ela.end(); it2++){
			auto i = it2 - spec->ela.begin();

			this->exportables.push_back(
				exportable{
					"k_ela_N_" + std::to_string(i),
					{ {"The density-reduced particular elastic collision rate coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_ela_N [m**3 s^-1]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"k_ela_N",
					col1_val,
					out.per_gas[Ng].k_ela_N[i] }
			);
		}

		// Particular effective rates
		for (auto it2 = spec->eff.begin(); it2 != spec->eff.end(); it2++) {
			auto i = it2 - spec->eff.begin();

			this->exportables.push_back(
				exportable{
					"k_eff_N_" + std::to_string(i),
					{ {"The density-reduced particular effective collision rate coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_eff_N [m**3 s^-1]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"k_eff_N",
					col1_val,
					out.per_gas[Ng].k_eff_N[i] }
			);
		}

		// Particular excitation rates
		for (auto it2 = spec->exc.begin();  it2 != spec->exc.end(); it2++) {
			auto i = it2 - spec->exc.begin();

			this->exportables.push_back(
				exportable{
					"k_exc_N_" + std::to_string(i),
					{ {"The density-reduced particular excitation collision rate coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_exc_N [m**3 s^-1]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"k_exc_N",
					col1_val,
					out.per_gas[Ng].k_exc_N[i] }
			);
		}

		// Particular ionization rates
		for (auto it2 = spec->iz.begin();  it2 != spec->iz.end(); it2++) {
			auto i = it2 - spec->iz.begin();

			this->exportables.push_back(
				exportable{
					"k_iz_N_" + std::to_string(i),
					{ {"The density-reduced particular ionization collision rate coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_iz_N [m**3 s^-1]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"k_iz_N",
					col1_val,
					out.per_gas[Ng].k_iz_N[i] }
			);


			this->exportables.push_back(
				exportable{
					"alpha_N_" + std::to_string(i),
					{ {"The density-reduced particular ionization growth coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: alpha_N [m**2]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"alpha_N",
					col1_val,
					out.per_gas[Ng].alpha_N[i] }
			);

		}

		// Particular superelastic rates
		for (auto it2 = spec->sup.begin();  it2 != spec->sup.end(); it2++) {
			auto i = it2 - spec->sup.begin();

			this->exportables.push_back(
				exportable{
					"k_sup_N_" + std::to_string(i),
					{ {"The density-reduced particular super-elastic collision rate coefficient."},
						{"Reactant: " + (*it2)->reactant()},
						{"Product: " + (*it2)->product()},
						{ "Process: " + (*it2)->process() },
						{ "col_1: " + col1_var + " " + col1_units + ", col_2: k_sup_N [m**3 s^-1]" }},
					GAS_LEVELS.at(Ng),
					col1_var,
					"k_sup_N",
					col1_val,
					out.per_gas[Ng].k_sup_N[i] }
			);
		}

		
	}


}

