// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// Used for debugging after I found some odd seg-fault errors related to collisionlibrary on Linux
// (found source of error: std::filesystem )

#include <collisionlibrary>

int main() {
    
    std::cout << "CollisionLibrary minimal file: Hello world!" << std::endl;
    
	return 0;
}






