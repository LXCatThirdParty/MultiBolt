:: MultiBolt v3.0.0
:: See same-named cpp file for details

cd ../../../bin

multibolt_win64.exe ^
--model HD+GE ^
--N_terms 6 ^
--p_Torr 760 ^
--T_K 300 ^
--EN_Td 100 ^
--Nu 200 ^
--initial_eV_max 100 ^
--USE_ENERGY_REMAP ^
--conv_err 1e-6 ^
--weight_f0 1.0 ^
--iter_max 100 ^
--iter_min 4 ^
--remap_target_order_span 10 ^
--remap_grid_trial_max 10 ^
--export_location "../Exported_MultiBolt_Data/" ^
--export_name "example_sweep_calculations_bin_frac/" ^
--LXCat_Xsec_fid "../cross-sections/Biagi_N2.txt" ^
--LXCat_Xsec_fid "../cross-sections/Biagi_Ar.txt" ^
--species "N2" 0.0 ^
--species "Ar" 1.0 ^
--sweep_option bin_frac ^
--sweep_style reg 0 0.2 1

pause